package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginCorrectoUsuario {
	WebDriver driver = new FirefoxDriver();
	
	@Given("^Introducir usuario y clave correcto$")
	public void introducir_usuario_y_clave_correcto() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("usuario1@todoApp.es");
		inputPass.sendKeys("123456789");
		Thread.sleep(1000);
		
	}
	
	@Given("^Introducir usuario o clave incorrecto$")
	public void introducir_usuario_o_clave_incorrecto() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("usuario1@todoApp.es");
		inputPass.sendKeys("1234");
		Thread.sleep(1000);
		
	}
	
	@When("^Pulsar Login$")
	public void pulsar_login() throws Throwable {
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		
	}
	
	@Then("^Acceder a pantalla principal de usuario$")
	public void acceder_a_pantalla_principal_de_usuario() throws Throwable {
		Thread.sleep(15000);
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/entrar.htm";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}

	}
	
	@Then("^Mostrar un Mensaje de Error$")
	public void mostrar_un_mensaje_de_error() throws Throwable {
		Thread.sleep(1000);
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}

	}

}
