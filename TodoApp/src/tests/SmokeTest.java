package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SmokeTest {
	WebDriver driver = new FirefoxDriver();
	//WebDriver driver = new ChromeDriver();
	
	@Given("^Abrimos Mozilla Firefox y nos conectamos a Google$")
	public void Abrimos_Mozilla_Firefox_y_nos_conectamos_a_Google() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://www.google.es");
		Thread.sleep(1000);
		
	}

	@When("^Insertamos \"([^\"]*)\"esi uclm\"([^\"]*)\" en la barra de busqueda$")
	public void Insertamos_esi_uclm_en_la_barra_de_busqueda(String arg1, String arg2) throws Throwable {
		WebElement barraBusqueda = driver.findElement(By.name("q"));
		barraBusqueda.sendKeys("esi uclm");
		barraBusqueda.submit();
		Thread.sleep(1000);
	}

	@When("^Le damos a buscar y nos saldran los resultados de la misma$")
	public void Le_damos_a_buscar_y_nos_saldran_los_resultados_de_la_misma() throws Throwable {
		WebElement botonBusqueda = driver.findElement(By.name("btnG"));
		botonBusqueda.click();
		Thread.sleep(1000);
	}
	
	@Then("^buscaremos el mejor resultado y entraremos en la pagina$")
	public void buscaremos_el_mejor_resultado_y_entraremos_en_la_pagina() throws Throwable {
		WebElement paginaWeb = driver.findElement(By.partialLinkText("Informática"));
		paginaWeb.click();
	}

	@Then("^cerraremos el navegador$")
	public void cerraremos_el_navegador() throws Throwable {
		Thread.sleep(7000);
	    driver.quit();
	}
	
}
