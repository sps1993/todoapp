package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class cerrarSesionUsuario {
	WebDriver driver = new FirefoxDriver();
	
	@Given("^el user se loguea correctamente$")
	public void el_user_se_loguea_correctamente() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("usuario1@todoApp.es");
		inputPass.sendKeys("123456789");
		Thread.sleep(1000);
	}

	@When("^aparece en la pantalla principal$")
	public void aparece_en_la_pantalla_principal() throws Throwable {
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(25000);
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/entrar.htm";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}
	}

	@Then("^pulsa en el boton de cerrar sesion$")
	public void pulsa_en_el_boton_de_cerrar_sesion() throws Throwable {
		WebElement botonCerrarSesion = driver.findElement(By.id("cerrarSesionPrincipalUsuarios"));
	    botonCerrarSesion.click();
	    Thread.sleep(1000);
	}

	@Then("^se desloguea y aparece en HOME$")
	public void se_desloguea_y_aparece_en_HOME() throws Throwable {
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/cerrarSesionUser.htm";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}
		Thread.sleep(1000);
	}
	
}
