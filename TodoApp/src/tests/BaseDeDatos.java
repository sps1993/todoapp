package tests;

import static org.junit.Assert.assertArrayEquals;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import modelo.DBGestionarProyectos;
import modelo.DBGestionarTarea;
import modelo.DBGestionarUsuario;
import modelo.DataBase;
import modelo.Usuario;

public class BaseDeDatos {
	Usuario userExiste, userNuevo, userNuevo2, userMenos8Caracteres, userCR;
	DBGestionarUsuario dbUsuarios;
	DBGestionarProyectos gP=new DBGestionarProyectos();
	DBGestionarTarea gT=new DBGestionarTarea();
	int resultado;
	String nombre, clave, email, rol, proyecto, nTarea, prioridad, fechaInicio, fechaFin, notas;
	boolean completada;
	
	@Given("^Un usuario se registra con email y clave$")
	public void Un_usuario_se_registra_con_email_y_clave() throws Throwable {
	    dbUsuarios=new DBGestionarUsuario();
	    userExiste=new Usuario("pepe@todoApp.es", "12345678");
		userNuevo=new Usuario("cuentaPrueba@todoApp.es", "123456789");
		userMenos8Caracteres=new Usuario("pepe2@todoApp.es", "1234");
		assert(true);
	}
	
	@Given("^Un usuario introduce email y clave$")
	public void Un_usuario_introduce_email_y_clave() throws Throwable {
		dbUsuarios=new DBGestionarUsuario();
		userExiste=new Usuario("pepe@todoApp.es", "12345678");
		userNuevo=new Usuario("cuentaPrueba@todoApp.es", "123456789");
		assert(true);
	}

	@Given("^Un usuario introduce mal email y clave$")
	public void Un_usuario_introduce_mal_email_y_clave() throws Throwable {
		dbUsuarios=new DBGestionarUsuario();
		userNuevo2=new Usuario("pepe@todoApp.es", "1234564444");
	}
	
	@Given("^El administrador esta autenticado$")
	public void El_administrador_esta_autenticado() throws Throwable {
		dbUsuarios=new DBGestionarUsuario();
	    userExiste=new Usuario("pepe@todoApp.es", "12345678");
		assert(dbUsuarios.autenticar(userExiste));
	}
	
	@When("^Autenticacion es correcta$")
	public void Autenticacion_es_correcta() throws Throwable {
		assert(dbUsuarios.autenticar(userExiste));
	}

	@Given("^Un usuario quiere autenticarse$")
	public void Un_usuario_quiere_autenticarse() throws Throwable {
		dbUsuarios=new DBGestionarUsuario();
		userNuevo=new Usuario("cuentaPrueba@todoApp.es", "");
		assert(true);
	}

	@When("^No recuerda la clave$")
	public void No_recuerda_la_clave() throws Throwable {
	    clave="";
	    assert(true);
	}

	@Then("^Recupera la clave$")
	public void Recupera_la_clave() throws Throwable {
	    clave=dbUsuarios.recuperarClave(userNuevo.getEmail());
	    assert(clave.equalsIgnoreCase("123456789"));
	}
	
	@When("^Busca a un usuario$")
	public void Busca_a_un_usuario() throws Throwable {
		userNuevo=new Usuario("cuentaPrueba@todoApp.es", "123456789");
		email=userNuevo.getEmail();
	}
	
	@Then("^Mostrar Mensaje OK$")
	public void Mostrar_Mensaje_OK() throws Throwable {
		if(resultado==1){assert(true);}	
	}

	@When("^Autenticacion es incorrecta$")
	public void Autenticacion_es_incorrecta() throws Throwable {
		assert(!dbUsuarios.autenticar(userNuevo2));	
	}

	@Then("^Mostrar Mensaje de Error$")
	public void Mostrar_Mensaje_de_Error() throws Throwable {
		if(resultado<0){assert(true);}
	}

	@When("^Pasa a ser administrador$")
	public void Pasa_a_ser_administrador() throws Throwable {
		rol="administrador";	
	}
	
	@Then("^Cambia el rol$")
	public void Cambia_el_rol_correctamente() throws Throwable {
		dbUsuarios.cambiarRol(userNuevo.getEmail(), rol);	
	}

	@Then("^Da de alta al usuario correctamente$")
	public void Da_de_alta_al_usuario_correctamente() throws Throwable {
		dbUsuarios.darDeAlta(email);		
	}

	@Then("^Se elimina sus proyectos y sus tareas$")
	public void Se_elimina_sus_proyectos_y_sus_tareas() throws Throwable {
		gP.borrarProyecto(userNuevo.getEmail());
		gT.borrarTareas(userNuevo.getEmail());
		if ((gP.nProyectos(userNuevo.getEmail())==0)&&(gT.nTareas(userNuevo.getEmail())==0)){
			assert(true);
		}else{assert(false);}		
	}
	
	@When("^Necesita un nuevo proyecto$")
	public void Necesita_un_nuevo_proyecto() throws Throwable {
		proyecto="Privado";
	}

	@Then("^Crea el Proyecto$")
	public void Crea_el_Proyecto() throws Throwable {
		gP.crearProyecto(userNuevo.getEmail(), proyecto);
	}

	@When("^Necesita una nueva tarea$")
	public void Necesita_una_nueva_tarea() throws Throwable {
		email=userNuevo.getEmail();
		nTarea="Fiesta Cumpleaņos";
		prioridad="Alta";
		proyecto="Privado";
		completada=false;
		fechaInicio="2016/12/12";
		fechaFin="2016/12/21";
		notas="comprar globos";
	}

	@Then("^Crea la Tarea$")
	public void Crea_la_Tarea() throws Throwable {
	    gT.createTarea(email, nTarea, prioridad, proyecto, completada, fechaInicio, fechaFin, notas);
	}

	@When("^El usuario elimina la cuenta$")
	public void El_usuario_elimina_la_cuenta_correctamente() throws Throwable {
		assert(dbUsuarios.deleteUsuario(userNuevo.getEmail()));	
	}

	@Given("^Un usuario esta autenticado$")
	public void Un_usuario_esta_autenticado() throws Throwable {
		dbUsuarios=new DBGestionarUsuario();
		userNuevo=new Usuario("cuentaPrueba@todoApp.es", "123456789");
		assert(dbUsuarios.autenticar(userNuevo));
	}

	@When("^El usuario modifica sus nombre$")
	public void El_usuario_modifica_su_nombre() throws Throwable {
		nombre="Nuevo";
	}

	
	@When("^Se modifica la cuenta$")
	public void Se_modifica_la_cuenta() throws Throwable {
		assert(dbUsuarios.updateUsuario(userNuevo.getEmail(), userNuevo.getClave(), nombre, "desc", "desc", "desc", "desc"));	
	}

	@When("^La cuenta se crea$")
	public void La_cuenta_se_crea() throws Throwable {
		resultado=dbUsuarios.createUsuario(userNuevo.getEmail(), userNuevo.getClave(), "desc", "desc", "desc", "desc", "desc", false);
		if (resultado==1){assert(dbUsuarios.autenticar(userNuevo));}	
	}

	@When("^La cuenta existe$")
	public void La_cuenta_existe() throws Throwable {
		resultado=dbUsuarios.createUsuario(userExiste.getEmail(), userExiste.getClave(), "desc", "desc", "desc", "desc", "desc", false);
	    if(resultado==-1){assert(dbUsuarios.autenticar(userExiste));}	
	}

	@When("^La clave tiene menos de (\\d+) caracteres$")
	public void La_clave_tiene_menos_de_caracteres(int arg1) throws Throwable {
		resultado=dbUsuarios.createUsuario(userMenos8Caracteres.getEmail(), userMenos8Caracteres.getClave(), "desc", "desc", "desc", "desc", "desc", false);
	    if(resultado==-2){assert(!dbUsuarios.autenticar(userMenos8Caracteres));}
	}
	
	@Given("^El administrador esta autenticado correctamente$")
	public void El_administrador_esta_autenticado_correctamente() throws Throwable {
		dbUsuarios=new DBGestionarUsuario();
	    userExiste=new Usuario("pepe@todoApp.es", "12345678");
		assert(dbUsuarios.autenticar(userExiste));
	}

	@When("^Busca a un usuario dado de Alta$")
	public void Busca_a_un_usuario_dado_de_Alta() throws Throwable {
		userNuevo=new Usuario("cuentaPrueba@todoApp.es", "123456789");
		email=userNuevo.getEmail();
	}

	@Then("^Da de baja al usuario correctamente$")
	public void Da_de_baja_al_usuario_correctamente() throws Throwable {
		dbUsuarios.darDeBaja(email);
	}
}