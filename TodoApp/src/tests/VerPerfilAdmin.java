package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VerPerfilAdmin {
	WebDriver driver = new FirefoxDriver();
	
	@Given("^Login admin correcto$")
	public void login_admin_correcto() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("pepe@todoApp.es");
		inputPass.sendKeys("12345678");
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(1000);
		
	}
	
	@When("^Pulsar ver perfil$")
	public void pulsar_ver_perfil() throws Throwable {
		WebElement botonPerfil = driver.findElement(By.id("verPerfilPrincipalAdmin"));
		botonPerfil.click();
		Thread.sleep(1000);
	}
	
	@Then("^Acceder a pantalla del perfil$")
	public void acceder_a_pantalla_principal_de_usuario() throws Throwable {
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/verPerfilAdmin.htm";
		
		if(urlActual.equals(urlEsperada)){
			
			assert(true);
		}

	}

}
