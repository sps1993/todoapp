package tests;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.web.servlet.ModelAndView;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RegistroTest {
	WebDriver driver = new FirefoxDriver();
	//Nota: S�lo se hace un escenario puesto que no importa si el registro es correcto o no, el comportamiento /
	//reflejado en estas prubas siempre se cumplir�, lo �nico que cambia es el emai
	
	@Given("^Un usuario pulsa el boton registrar$")
	public void Un_usuario_pulsa_el_boton_registrar() {
		driver.get("http://localhost:8781/TodoApp/");
		WebElement botonRegistro = driver.findElement(By.id("botonRegistrar"));
		botonRegistro.click();
	    // Express the Regexp above with the code you wish you had

	}

	@When("^Pulsa el boton aceptar tras introducir sus datos$")
	public void Pulsa_el_boton_aceptar_tras_introducir_sus_datos() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // Express the Regexp above with the code you wish you had
		WebElement email = driver.findElement(By.id("userEmailRegRegistrar"));
		WebElement contra = driver.findElement(By.id("contrasenaRegRegistrar"));
		WebElement contra2 = driver.findElement(By.id("contrasena2RegRegistrar"));
		WebElement botonRegistro = driver.findElement(By.id("aceptarRegistrar"));
		email.sendKeys("emailTest@prueba.es");
		contra.sendKeys("prueba");
		contra2.sendKeys("prueba");
		botonRegistro.click();
		
	    
	}

	@Then("^Se vuelve a la vista de login$")
	public void Se_vuelve_a_la_vista_de_login_mostrando_el_mensaje_usuario_registrado_correctamente() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assert(driver.getCurrentUrl().equals("http://localhost:8781/TodoApp/reg.htm"));
	    // Express the Regexp above with the code you wish you had 
	    
	}


}
