package tests;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.web.servlet.ModelAndView;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class modificarPerfilTest {
	WebDriver driver = new FirefoxDriver();

	
	
	@Given("^Un administrador pulsa modificar dentro de su perfil$")
	public void Un_administrador_pulsa_modificar_dentro_de_su_perfil() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8781/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("pepe@todoApp.es");
		inputPass.sendKeys("12345678");
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(3000);
		
		driver.get("http://localhost:8781/TodoApp/verPerfilAdmin.htm");
		WebElement botonMod = driver.findElement(By.id("modificarMiPerfil"));
		botonMod.click();
	    // Express the Regexp above with the code you wish you had

	}

	@When("^Modifica un dato$")
	public void Modifica_un_dato() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // Express the Regexp above with the code you wish you had
		WebElement cambiar = driver.findElement(By.id("inputApellidosModificarPerfil"));
		WebElement botonMod = driver.findElement(By.id("guardarModificarPerfil"));
		cambiar.sendKeys("Apellidos");
		botonMod.click();
		
	    
	}

	@Then("^Se vuelve al perfil$")
	public void Se_vuelve_al_perfil() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assert(driver.getCurrentUrl().equals("http://localhost:8781/TodoApp/perfilModificar.htm"));
	    // Express the Regexp above with the code you wish you had 
	    
	}
	
	@Given("^Un usuario pulsa modificar dentro de su perfil$")
	public void Un_usuario_pulsa_modificar_dentro_de_su_perfil() {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8781/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("usuario2@todoApp.es");
		inputPass.sendKeys("123456789");
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.get("http://localhost:8781/TodoApp/verPerfilo.htm");
		WebElement botonMod = driver.findElement(By.id("modificarMiPerfilUsuario"));
		botonMod.click();
	    // Express the Regexp above with the code you wish you had

	}

	@When("^Modifica un dato de usuario$")
	public void Modifica_un_dato_de_usuario() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // Express the Regexp above with the code you wish you had
		WebElement cambiar = driver.findElement(By.id("inputApellidosModificarPerfilUsuario"));
		WebElement botonMod = driver.findElement(By.id("guardarModificarPerfilUsuario"));
		cambiar.sendKeys("Apellidos");
		botonMod.click();
		
	    
	}

	@Then("^Se vuelve al perfil de usuario$")
	public void Se_vuelve_al_perfil_de_usuario() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assert(driver.getCurrentUrl().equals("http://localhost:8781/TodoApp/perfilUsuarioModificar.htm"));
	    // Express the Regexp above with the code you wish you had 
	    
	}


}
