package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CambiarRolTest {
WebDriver driver = new FirefoxDriver();
	
	@Given("^Seleccionar uruario y rol$")
	public void seleccionar_usuario_y_rol() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("pepe@todoApp.es");
		inputPass.sendKeys("12345678");
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(5000);
		WebElement botonRol = driver.findElement(By.id("cambiarRolesPrincipalAdmin"));
		botonRol.click();
		Thread.sleep(5000);	
		WebElement checkUsuario=driver.findElement(By.name("checkbox1"));
		checkUsuario.click();
		WebElement rolAdmin=driver.findElement(By.id("admin"));
		rolAdmin.click();
		
	}
	
	@When("^Pulsar cambiar rol$")
	public void pulsar_cambiar_rol() throws Throwable {
		WebElement botonRol = driver.findElement(By.id("guardarCambiarRoles"));
		botonRol.click();
		Thread.sleep(5000);
	}
	
	@Then("^El usuario ha cambiado de rol$")
	public void el_usuario_ha_cambiado_de_rol() throws Throwable {
		//

	}

}
