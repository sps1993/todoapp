package tests;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.web.servlet.ModelAndView;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

//Nota: no importa si el email es v�lido o no, permanecer� en la p�gina.
public class RecuperarContraTest {
	WebDriver driver = new FirefoxDriver();

	
	
	@Given("^Un usuario pulsa recuperar contrasena$")
	public void Un_usuario_pulsa_recuperar_contrasena() {
		driver.get("http://localhost:8781/TodoApp/");
		WebElement botonContra = driver.findElement(By.id("RecuperarContrasena"));
		botonContra.click();
	    // Express the Regexp above with the code you wish you had

	}

	@When("^Introduce un correo$")
	public void Introduce_un_correo() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // Express the Regexp above with the code you wish you had
		WebElement email = driver.findElement(By.id("inputEmailRecuperarContrasena"));
		WebElement botonContra = driver.findElement(By.id("recuperarClaveRecuperarContrasena"));
		email.sendKeys("emailTest@prueba.es");
		botonContra.click();
		
	    
	}

	@Then("^Se permanece en la pagina de recuperar contrasena$")
	public void Se_permanece_en_la_pagina_de_recuperar_contrasena() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assert(driver.getCurrentUrl().equals("http://localhost:8781/TodoApp/RecuperarContrase%C3%B1a.htm"));
	    // Express the Regexp above with the code you wish you had 
	    
	}


}
