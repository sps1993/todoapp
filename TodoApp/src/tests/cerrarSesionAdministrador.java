package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class cerrarSesionAdministrador {
	WebDriver driver = new FirefoxDriver();

	@Given("^el admin se loguea correctamente$")
	public void Login_admin_correcto() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("pepe@todoApp.es");
		inputPass.sendKeys("12345678");
		Thread.sleep(2000);
	}

	@When("^aparece su pantalla principal$")
	public void aparece_su_pantalla_principal() throws Throwable {
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(6000);
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/entrar.htm";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}
	}

	@Then("^pulsa en cerrar sesion$")
	public void pulsa_en_cerrar_sesion() throws Throwable {
	    WebElement botonCerrarSesion = driver.findElement(By.id("cerrarSesionPrincipalAdmin"));
	    botonCerrarSesion.click();
	    Thread.sleep(1000);
	}

	@Then("^se desconecta y aparece en la pantalla de inicio$")
	public void se_desconecta_y_aparece_en_la_pantalla_de_inicio() throws Throwable {
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/index.htm";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}
		Thread.sleep(1000);
	}
	
}
