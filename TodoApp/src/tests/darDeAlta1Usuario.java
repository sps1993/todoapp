package tests;

import java.util.LinkedList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import modelo.DBGestionarUsuario;
import modelo.Usuario;

public class darDeAlta1Usuario {
	WebDriver driver = new FirefoxDriver();
	String usuarioDadoAlta, usuarioDadoAlta2;
	
	@Given("^El admin introduce su email y pass$")
	public void El_admin_introduce_su_email_y_pass() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("pepe@todoApp.es");
		inputPass.sendKeys("12345678");
		Thread.sleep(1000);
	}

	@When("^Pulsar en entrar$")
	public void Pulsar_en_entrar() throws Throwable {
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(2000);
	}

	/* Este when ya esta implementado en LoginAdministrador
	 * @When("^Acceder a pantalla principal de administrador$")
	 
	public void Acceder_a_pantalla_principal_de_administrador() throws Throwable {
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/entrar.htm";
		
		if(urlActual.equals(urlEsperada)){
			("Entra");
			assert(true);
			
		}
	}*/

	@When("^pulsa en darAlta$")
	public void pulsa_en_darAlta() throws Throwable {
	    WebElement botonDarAlta = driver.findElement(By.id("darAltasPrincipalAdmin"));
	    botonDarAlta.click();
	    Thread.sleep(7000);
	}

	@Then("^selecciona un usuario$")
	public void selecciona_un_usuario() throws Throwable {
	    WebElement checkBoxUsuario = driver.findElement(By.id("checkbox0"));
	    
	    checkBoxUsuario.click();
	    Thread.sleep(1000);
	}

	@Then("^le da de Alta$")
	public void le_da_de_Alta() throws Throwable {
	    WebElement botonDarAlta = driver.findElement(By.id("darDeAltaUsuarioDarDeAlta"));	
	    
	    /**Obtenemos el usuario dado de Alta**/
	    DBGestionarUsuario usuarios = new DBGestionarUsuario();
	    LinkedList<Usuario> users =usuarios.readUsuarios();
	    LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
	    String checkBoxActual="checkbox0";
		String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
		int numeroFila = Integer.parseInt(filaTabla);
		String []datosUsuario=listaUsuariosVista.get(numeroFila);
		usuarioDadoAlta=datosUsuario[0];
		
		/**Le damos de Alta**/
		botonDarAlta.click();
	    Thread.sleep(9000);
	    
	    /**Comprobamos si ese usuario est� dado de Alta**/
	    for(int k=0; k<users.size();k++){
			Usuario u = users.get(k);
			if(u.getEmail().equals(usuarioDadoAlta)){
				assert(usuarios.getDadoAlta(u));
			}
			
		}
	    Thread.sleep(2000);
	    
	}
	
	@Given("^El admin introduce su email y password$")
	public void El_admin_introduce_su_email_y_password() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("pepe@todoApp.es");
		inputPass.sendKeys("12345678");
		Thread.sleep(1000);
	}

	@When("^Pulsar en entrar para loguearse$")
	public void Pulsar_en_entrar_para_loguearse() throws Throwable {
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(2000);
	}

	@When("^Accede a su pantalla principal de administrador$")
	public void Accede_a_su_pantalla_principal_de_administrador() throws Throwable {
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/entrar.htm";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}
	}

	@When("^pulsa en Altas de Usuarios$")
	public void pulsa_en_Altas_de_Usuarios() throws Throwable {
		WebElement botonDarAlta = driver.findElement(By.id("darAltasPrincipalAdmin"));
	    botonDarAlta.click();
	    Thread.sleep(7000);
	}

	@Then("^selecciona varios usuario$")
	public void selecciona_varios_usuario() throws Throwable {
		WebElement checkBoxUsuario = driver.findElement(By.id("checkbox0"));
	    checkBoxUsuario.click();
	    WebElement checkBoxUsuario2 = driver.findElement(By.id("checkbox1"));
	    checkBoxUsuario2.click();
	    Thread.sleep(1000);
	}

	@Then("^les da de Alta$")
	public void les_da_de_Alta() throws Throwable {
		WebElement botonDarAlta = driver.findElement(By.id("darDeAltaUsuarioDarDeAlta"));	
	    
	    /**Obtenemos el usuario dado de Alta**/
	    DBGestionarUsuario usuarios = new DBGestionarUsuario();
	    LinkedList<Usuario> users =usuarios.readUsuarios();
	    LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
	    LinkedList<String> listaCheckbox= new LinkedList<String>();
	    String checkBoxActual="checkbox0";
	    String checkBoxActual2="checkbox1";
	    listaCheckbox.add(checkBoxActual);
	    listaCheckbox.add(checkBoxActual2);
		String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
		int numeroFila = Integer.parseInt(filaTabla);
		String []datosUsuario=listaUsuariosVista.get(numeroFila);
		usuarioDadoAlta=datosUsuario[0];
		String filaTabla2 = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
		int numeroFila2 = Integer.parseInt(filaTabla2);
		String []datosUsuario2=listaUsuariosVista.get(numeroFila2);
		usuarioDadoAlta2=datosUsuario2[0];
		
		/**Les damos de Alta**/
		botonDarAlta.click();
	    Thread.sleep(9000);
	    
	    /**Comprobamos si esos usuarios est�n dados de Alta**/
	    boolean usuario1 = false,usuario2 = false;
	    for(int k=0; k<users.size();k++){
			Usuario u = users.get(k);
			
			if(u.getEmail().equals(usuarioDadoAlta)) usuario1=usuarios.getDadoAlta(u);
			
		}
	    
	    for(int x=0; x<users.size();x++){
			Usuario p = users.get(x);
			
			if(p.getEmail().equals(usuarioDadoAlta2)) usuario2=usuarios.getDadoAlta(p);
			
		}
	    assert(usuario1 && usuario2);
	    Thread.sleep(2000);
	}
	
}
