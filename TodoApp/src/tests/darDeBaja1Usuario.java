package tests;

import java.util.LinkedList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import modelo.DBGestionarUsuario;
import modelo.Usuario;

public class darDeBaja1Usuario {

	WebDriver driver = new FirefoxDriver();
	String usuarioDadoBaja, usuarioDadoBaja2;
	
	@Given("^El admin se loguea en el sistema$")
	public void El_admin_se_loguea_en_el_sistema() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("pepe@todoApp.es");
		inputPass.sendKeys("12345678");
		Thread.sleep(1000);
	}

	@When("^Pulsar en el boton ENTRAR$")
	public void Pulsar_en_el_bot_n_ENTRAR() throws Throwable {
	    WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(5000);
	}

	@When("^Acceder al sistema$")
	public void Acceder_al_sistema() throws Throwable {
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/entrar.htm";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}
	}

	@When("^pulsa en darBaja$")
	public void pulsa_en_darBaja() throws Throwable {
		WebElement botonDarBaja = driver.findElement(By.id("darBajasPrincipalAdmin"));
	    botonDarBaja.click();
	    Thread.sleep(10000);
	}

	@Then("^selecciona un usuario dado de alta$")
	public void selecciona_un_usuario_dado_de_alta() throws Throwable {
		WebElement checkBoxUsuario = driver.findElement(By.id("checkbox0"));
	    
	    checkBoxUsuario.click();
	    Thread.sleep(1500);
	}

	@Then("^le da de Baja$")
	public void le_da_de_Baja() throws Throwable {
		WebElement botonDarBaja = driver.findElement(By.id("darDeBajaUsuarioDarDeBaja"));	
	    
	    /**Obtenemos el usuario dado de Baja**/
	    DBGestionarUsuario usuarios = new DBGestionarUsuario();
	    LinkedList<Usuario> users =usuarios.readUsuarios();
	    LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
	    String checkBoxActual="checkbox0";
		String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
		int numeroFila = Integer.parseInt(filaTabla);
		String []datosUsuario=listaUsuariosVista.get(numeroFila);
		usuarioDadoBaja=datosUsuario[0];
		
		/**Le damos de Baja**/
		botonDarBaja.click();
	    Thread.sleep(9000);
	    
	    /**Comprobamos si ese usuario est� dado de Baja**/
	    for(int k=0; k<users.size();k++){
			Usuario u = users.get(k);
			if(u.getEmail().equals(usuarioDadoBaja)){
				assert(usuarios.getDadoAlta(u)==false);
			}
			
		}
	    Thread.sleep(4000);
	}

	@Given("^El admin realiza login$")
	public void El_admin_realiza_login() throws Throwable {
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		WebElement inputEmail = driver.findElement(By.id("userEmailLogin"));
		WebElement inputPass = driver.findElement(By.id("contrasenaLogin"));
		inputEmail.sendKeys("pepe@todoApp.es");
		inputPass.sendKeys("12345678");
		Thread.sleep(1000);
	}

	@When("^se loguea pulsando el boton entrar$")
	public void se_loguea_pulsando_el_bot_n_entrar() throws Throwable {
		WebElement botonEntrar = driver.findElement(By.id("botonEntrar"));
		botonEntrar.click();
		Thread.sleep(7000);
	}

	@When("^Accede a su pantalla$")
	public void Accede_a_su_pantalla() throws Throwable {
		String urlActual=driver.getCurrentUrl();
		String urlEsperada="http://localhost:8080/TodoApp/entrar.htm";
		
		if(urlActual.equals(urlEsperada)){
			assert(true);
			
		}
	}

	@When("^pulsa en Bajas de Usuarios$")
	public void pulsa_en_Bajas_de_Usuarios() throws Throwable {
		WebElement botonDarBaja = driver.findElement(By.id("darBajasPrincipalAdmin"));
	    botonDarBaja.click();
	    Thread.sleep(5000);
	}

	@Then("^selecciona varios usuarios$")
	public void selecciona_varios_usuarios() throws Throwable {
		WebElement checkBoxUsuario = driver.findElement(By.id("checkbox0"));
	    checkBoxUsuario.click();
	    WebElement checkBoxUsuario2 = driver.findElement(By.id("checkbox1"));
	    checkBoxUsuario2.click();
	    Thread.sleep(2000);
	}

	@Then("^les da de Baja$")
	public void les_da_de_Baja() throws Throwable {
		WebElement botonDarBaja = driver.findElement(By.id("darDeBajaUsuarioDarDeBaja"));	
	    
	    /**Obtenemos el usuario dado de Alta**/
	    DBGestionarUsuario usuarios = new DBGestionarUsuario();
	    LinkedList<Usuario> users =usuarios.readUsuarios();
	    LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
	    LinkedList<String> listaCheckbox= new LinkedList<String>();
	    String checkBoxActual="checkbox0";
	    String checkBoxActual2="checkbox1";
	    listaCheckbox.add(checkBoxActual);
	    listaCheckbox.add(checkBoxActual2);
		String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
		int numeroFila = Integer.parseInt(filaTabla);
		String []datosUsuario=listaUsuariosVista.get(numeroFila);
		usuarioDadoBaja=datosUsuario[0];
		String filaTabla2 = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
		int numeroFila2 = Integer.parseInt(filaTabla2);
		String []datosUsuario2=listaUsuariosVista.get(numeroFila2);
		usuarioDadoBaja2=datosUsuario2[0];
		
		/**Les damos de Alta**/
		botonDarBaja.click();
	    Thread.sleep(9000);
	    
	    /**Comprobamos si esos usuarios est�n dados de Alta**/
	    boolean usuario1 = false,usuario2 = false;
	    for(int k=0; k<users.size();k++){
			Usuario u = users.get(k);
			
			if(u.getEmail().equals(usuarioDadoBaja)) usuario1=usuarios.getDadoAlta(u);
			
		}
	    
	    for(int x=0; x<users.size();x++){
			Usuario p = users.get(x);
			
			if(p.getEmail().equals(usuarioDadoBaja2)) usuario2=usuarios.getDadoAlta(p);
			
		}
	    assert(usuario1==false && usuario2==false);
	    Thread.sleep(2000);
	}
	
}
