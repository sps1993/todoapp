package tests;

import org.apache.catalina.startup.Tomcat;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.gargoylesoftware.htmlunit.javascript.host.file.File;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TomcatTest {
	
	private Tomcat mTomcat;
	private String mWorkingDir = System.getProperty("java.io.tmpdir");
	WebDriver driver = new FirefoxDriver();
	
	@Given("^La herramienta tomcat se inicia.$")
	public void La_herramienta_tomcat_se_inicia() throws Throwable {
	    // Express the Regexp above with the code you wish you had
		mTomcat = new Tomcat();
		
		mTomcat.setPort(0);
		mTomcat.setBaseDir(mWorkingDir);
		mTomcat.getHost().setAppBase(mWorkingDir);
		mTomcat.getHost().setAutoDeploy(true);
		mTomcat.getHost().setDeployOnStartup(true);
	    
	    
	    
	}

	@When("^tomcat esta funcionando$")
	public void tomcat_esta_funcionando() throws Throwable {
	    // Express the Regexp above with the code you wish you had
		//Se arranca el tomcat.
		String contextPath = "/" + "TodoApp";
		mTomcat.start();
		
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("http://localhost:8080/TodoApp/");
		Thread.sleep(1000);
		
		WebElement barraBusqueda = driver.findElement(By.name("userEmailLogin"));
		barraBusqueda.sendKeys("pepe@todoApp.es");
		barraBusqueda = driver.findElement(By.name("contrasenaLogin"));
		barraBusqueda.sendKeys("12345678");
		barraBusqueda.submit();
		Thread.sleep(1000);
	   
	}

	@Then("^tomcat se para$")
	public void tomcat_se_para() throws Throwable {
	    // Express the Regexp above with the code you wish you had
		WebElement botonCerrarSesion = driver.findElement(By.id("cerrarSesionPrincipalAdmin"));
	    botonCerrarSesion.click();
	    Thread.sleep(3000);
	    
		mTomcat.stop();
	   
	}



}
