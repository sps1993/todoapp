package modelo;

public class Usuario {
	String email, clave;

	public Usuario(String em, String pwd){
		this.email=em;
		this.clave=pwd;
	}
	
	public String getEmail(){return this.email;}
	public String getClave(){return this.clave;}
	public void setEmail(String email){this.email = email;}
	public void setClave(String clave){this.clave = clave;}
}
