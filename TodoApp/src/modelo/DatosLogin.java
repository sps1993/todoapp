package modelo;

public class DatosLogin {
	
	DBGestionarUsuario mInstancia;
	
	public DatosLogin(){
		mInstancia=DBGestionarUsuario.getInstance();
	}

	public String getEmailLogin(){
		return mInstancia.emailLogin;
	}

	public void setEmailLogin(String email){
		mInstancia.emailLogin=email;
	}
	
}
