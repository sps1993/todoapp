package modelo;

import java.util.LinkedList;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;

public class DataBase {
	MongoClient cliente;
	DB todoApp;
	DBCollection usuarios, tareas, proyectos;
	DBObject documento, documento2;
	BasicDBObject document;
	static DataBase mInstancia;
	
	private DataBase(){
		String textUri = "mongodb://admin:admin@ds111788.mlab.com:11788/todoapp";
		MongoClientURI uri  = new MongoClientURI(textUri); 
        cliente = new MongoClient(uri);
        todoApp = cliente.getDB(uri.getDatabase());
		usuarios=todoApp.getCollection("usuarios");
		tareas=todoApp.getCollection("tareas");
		proyectos=todoApp.getCollection("proyectos");
	}
	
	public static DataBase getInstance(){
		if(mInstancia==null){
			mInstancia=new DataBase();
		}
		return mInstancia;
	}
	
	public boolean createUsuario(DBObject document){
		usuarios.insert(document);
		if(usuarios.find(document).count()==1){
			return true;
		}else{return false;}
	}
	
	public boolean createTarea(DBObject document){
		tareas.insert(document);
		if(tareas.find(document).count()==1){
			return true;
		}else{return false;}
	}
	
	public DBCursor readUsuario(DBObject document){
		return usuarios.find(document);
	}
	
	public DBCursor readTarea(DBObject document){
		return tareas.find(document);
	}
	
	public DBCursor readTareaOrdenadaPorFecha(){
		return tareas.find(document).sort(new BasicDBObject("fechaFin", 1));
	}
	
	public DBCursor readTareaOrdenadaPorPrioridad(){
		return tareas.find(document).sort(new BasicDBObject("prioridad", 1));
	}
	
	public DBCursor readTareaOrdenadaPorProyecto(){
		return tareas.find(document).sort(new BasicDBObject("proyecto", 1));
	}
	
	public DBCursor readAllUsuario(){
		return usuarios.find();
	}
	
	public DBCursor readAllTareas(){
		return tareas.find();
	}
	
	public boolean updateUsuario(DBObject docAntiguo, DBObject docNuevo){
		usuarios.update(docAntiguo, docNuevo);
		if(usuarios.find(docNuevo).count()==1 && usuarios.find(docAntiguo).count()==0){
			return true;
		}
		else {return false;}
	}
	
	public boolean updateTarea(DBObject docAntiguo, DBObject docNuevo){
		tareas.update(docAntiguo, docNuevo);
		if(tareas.find(docNuevo).count()==1 && tareas.find(docAntiguo).count()==0){
			return true;
		}
		else {return false;}
	}
	
	public boolean deleteUsuario(DBObject document){
		usuarios.remove(document);
		if(usuarios.find(document).count()==0){
			return true;
		}else{
			return false;
		}
	}
	
	public void deleteTarea(DBObject document){
		tareas.remove(document);
	}
	
	public void deleteProyecto(DBObject document){
		proyectos.remove(document);
	}
	
	public boolean createProyecto(DBObject document){
		proyectos.insert(document);
		if(proyectos.find(document).count()==1)return true;
		else return false;
	}
	
	public LinkedList<DBObject> readProyecto(DBObject document){
		LinkedList<DBObject> listaProyectos=new LinkedList<DBObject>();
		DBCursor pj=proyectos.find(document);
		while(pj.hasNext()){
			listaProyectos.add(pj.next());
		}
		return listaProyectos;
	}
	
	public void cerrarBD(){
		mInstancia=new DataBase();
		cliente.close();
	}
}
