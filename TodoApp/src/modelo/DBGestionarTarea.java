package modelo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBGestionarTarea {
	static DBGestionarTarea mInstancia;
	DataBase mInstance;
	DBObject document;
	DBCursor dc;
	String tareaInfo;
	
	public DBGestionarTarea(){
		mInstance=DataBase.getInstance();
	}
	
	public static DBGestionarTarea getInstance(){
		if(mInstancia==null){
			mInstancia=new DBGestionarTarea();
		}
		return mInstancia;
	}
	
	public int createTarea(String email, String nTarea, String prioridad, String proyecto, boolean completada, String fechaInicio, String fechaFin, String notas){
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nTarea);
		document.put("prioridad", prioridad);
		document.put("proyecto", proyecto);
		document.put("completada", completada);
		document.put("fechaInicio", fechaInicio);
		document.put("fechaFin", fechaFin);
		document.put("notas",  notas);
		mInstance.createTarea(document);
		return 1;
	}
	
	public int nTareas(String email){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		return mInstance.readTarea(document).size();
	}
	
	public void borrarTareas(String email){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		mInstance.deleteTarea(document);
	}
	
	public boolean updateTarea(String email, String nTarea, String prioridad, String proyecto, boolean completada, String fechaInicio, String fechaFin, String notas){
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nTarea);
		dc=mInstance.readTarea(document);
		if(dc.count()==1){
			DBObject antiguo=dc.next();
			document=new BasicDBObject();
			document.put("email", email);
			document.put("nombreTarea", nTarea);
			document.put("prioridad", prioridad);
			document.put("proyecto", proyecto);
			document.put("completada", completada);
			document.put("fechaInicio", fechaInicio);
			document.put("fechaFin", fechaFin);
			document.put("notas",  notas);
			mInstance.updateTarea(antiguo, document);
			return true;
		}else return false;
	}

	public LinkedList<Tarea> readTareas(){
		DBCursor users=mInstance.readAllTareas();
		LinkedList<Tarea> tareas=new LinkedList<Tarea>();
		DBObject t;
		for (int i=0;i<users.size();i++){
			t=users.next();
			Tarea tareaAñadida = new Tarea(t.get("email").toString(), t.get("nombreTarea").toString(), t.get("prioridad").toString(), t.get("proyecto").toString(), t.get("fechaInicio").toString(), t.get("fechaFin").toString(), t.get("notas").toString(), (boolean)t.get("completada"));
			tareas.add(tareaAñadida);
		}
		return tareas;
	}
	
	public LinkedList<Tarea> readTareasOrdenadasPorFecha(){
		DBCursor users=mInstance.readTareaOrdenadaPorFecha();
		LinkedList<Tarea> tareas=new LinkedList<Tarea>();
		DBObject t;
		while(users.hasNext()){
			t=users.next();
			tareas.add(new Tarea(t.get("email").toString(), t.get("nombreTarea").toString(), t.get("prioridad").toString(), t.get("proyecto").toString(), t.get("fechaInicio").toString(), t.get("fechaFin").toString(), t.get("notas").toString(), (boolean)t.get("completada")));
		}
		return tareas;
	}
	
	public LinkedList<Tarea> readTareasOrdenadasPorProyecto(){
		DBCursor users=mInstance.readTareaOrdenadaPorProyecto();
		LinkedList<Tarea> tareas=new LinkedList<Tarea>();
		DBObject t;
		while(users.hasNext()){
			t=users.next();
			tareas.add(new Tarea(t.get("email").toString(), t.get("nombreTarea").toString(), t.get("prioridad").toString(), t.get("proyecto").toString(), t.get("fechaInicio").toString(), t.get("fechaFin").toString(), t.get("notas").toString(), (boolean)t.get("completada")));
		}
		return tareas;
	}
	
	public LinkedList<Tarea> readTareasOrdenadasPorPrioridad(){
		DBCursor users=mInstance.readTareaOrdenadaPorPrioridad();
		LinkedList<Tarea> tareas=new LinkedList<Tarea>();
		DBObject t;
		while(users.hasNext()){
			t=users.next();
			tareas.add(new Tarea(t.get("email").toString(), t.get("nombreTarea").toString(), t.get("prioridad").toString(), t.get("proyecto").toString(), t.get("fechaInicio").toString(), t.get("fechaFin").toString(), t.get("notas").toString(), (boolean)t.get("completada")));
		}
		return tareas;
	}
	
	public Tarea readTarea(String email, String nombreTarea){
		Tarea t;
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nombreTarea);
		DBCursor tarea=mInstance.readTarea(document);
		DBObject ta=tarea.next();
		t=new Tarea(ta.get("email").toString(), ta.get("nombreTarea").toString(), ta.get("prioridad").toString(), ta.get("proyecto").toString(), ta.get("fechaInicio").toString(), ta.get("fechaFin").toString(), ta.get("notas").toString(), (boolean)ta.get("completada"));
		return t;
	}
	
	public void deleteTarea(String email, String nombreTarea){
		document=new BasicDBObject();
		document.put("email", email);
		document.put("nombreTarea", nombreTarea);
		mInstance.deleteTarea(document);
	}

	public void desconectar(){
		mInstance.cerrarBD();
	}
	
public LinkedList<String[]> crearTablaTareasCompletadas(LinkedList<Tarea> tasks, String email) {
				
		
		LinkedList<String[]> listaTareas =new LinkedList<String[]>();
		
		
		while(!tasks.isEmpty()){
			
			if(tasks.peek().getEmail().equals(email) && tasks.peek().isCompletada()){
				String[] tarea = new String[4];
				tarea[0] = tasks.peek().getnTarea();
				tarea[1] = tasks.peek().getFechaFin();
				tarea[2] = tasks.peek().getProyecto();
				tarea[3] = tasks.peek().getPrioridad();
				listaTareas.add(tarea);
			}
			tasks.remove();
		}
		
		
		return listaTareas;
	}

public LinkedList<String[]> crearTablaTareasActuales(LinkedList<Tarea> tasks, String email) {
	
	
	LinkedList<String[]> listaTareas =new LinkedList<String[]>();
	
	Date fechaactual = new Date();
    SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy/MM/dd");
	
	
	while(!tasks.isEmpty()){
		
		Date fechainicio;
		boolean correcto=false;
		try {
			fechainicio = dmyFormat.parse(tasks.peek().getFechaInicio());
			
			if(fechaactual.after(fechainicio)){ //se comprueba que la fecha actual es anterior a la fecha de inicio
				correcto=true;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(tasks.peek().getEmail().equals(email) && !tasks.peek().isCompletada() && correcto){
			String[] tarea = new String[4];
			tarea[0] = tasks.peek().getnTarea();
			tarea[1] = tasks.peek().getFechaFin();
			tarea[2] = tasks.peek().getProyecto();
			tarea[3] = tasks.peek().getPrioridad();
			listaTareas.add(tarea);
		}
		tasks.remove();
	}
	
	
	return listaTareas;
}

public LinkedList<String[]> crearTablaTareasFuturas(LinkedList<Tarea> tasks, String email) {
	
	
	LinkedList<String[]> listaTareas =new LinkedList<String[]>();
	
	Date fechaactual = new Date();
    SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy/MM/dd");
    
	
	
	while(!tasks.isEmpty()){
		
		Date fechainicio;
		boolean correcto=false;
		try {
			fechainicio = dmyFormat.parse(tasks.peek().getFechaInicio());
			if(fechaactual.before(fechainicio)){ //se comprueba que la fecha actual es anterior a la fecha de inicio
				correcto=true;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(tasks.peek().getEmail().equals(email) && !tasks.peek().isCompletada() && correcto){
			String[] tarea = new String[4];
			tarea[0] = tasks.peek().getnTarea();
			tarea[1] = tasks.peek().getFechaFin();
			tarea[2] = tasks.peek().getProyecto();
			tarea[3] = tasks.peek().getPrioridad();
			listaTareas.add(tarea);
		}
		tasks.remove();
	}
	
	
	return listaTareas;
}

public String getNTareaInfo(){ return tareaInfo; }
	
	
public void setNTareaInfo(String nTarea){ tareaInfo=nTarea; }

}
