package modelo;

import java.util.LinkedList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBGestionarUsuario {
	
	static DBGestionarUsuario mInstancia;
	DataBase mInstance;
	DBObject document;
	DBCursor dc;
	String emailLogin;
	DBGestionarProyectos gP=new DBGestionarProyectos();
	
	public DBGestionarUsuario(){
		mInstance=DataBase.getInstance();
	}
	
	public static DBGestionarUsuario getInstance(){
		if(mInstancia==null){
			mInstancia=new DBGestionarUsuario();
		}
		return mInstancia;
	}
	
	public int createUsuario(String user, String clave, String nombre, String apellidos, String rol, String direccion, String telefono, boolean dadoAlta){
			document=new BasicDBObject();
			document.put("usuario", user);
			dc=mInstance.readUsuario(document);
			if(dc.count()==1) return -1;
			else{
				document.put("clave", clave);
				document.put("nombre", nombre);
				document.put("apellidos", apellidos);
				document.put("rol", rol);
				document.put("direccion", direccion);
				document.put("telefono", telefono);
				document.put("dadoAlta", dadoAlta);
				mInstance.createUsuario(document);
				gP.crearProyecto(user, "Bandeja de Entrada");
				return 1;
			}
		}
	
	public boolean updateUsuario(String user, String clave, String nombre, String apellidos, String rol, String direccion, String telefono){
		if(clave.length()>=8){
			document=new BasicDBObject();
			document.put("usuario", user);
			dc=mInstance.readUsuario(document);
			if(dc.count()==1){
				DBObject antiguo=dc.next();
				document=new BasicDBObject();
				document.put("usuario", user);
				document.put("clave", clave);
				document.put("nombre", nombre);
				document.put("apellidos", apellidos);
				document.put("rol", rol);
				document.put("direccion", direccion);
				document.put("telefono", telefono);
				document.put("dadoAlta", antiguo.get("dadoAlta"));
				mInstance.updateUsuario(antiguo, document);
				return true;
			}else return false;
		}else return false;
	}

	public LinkedList<Usuario> readUsuarios(){
		DBCursor users=mInstance.readAllUsuario();
		LinkedList<Usuario> usuarios=new LinkedList<Usuario>();
		DBObject u;
		while(users.hasNext()){
			u=users.next();
			usuarios.add(new Usuario(u.get("usuario").toString(), u.get("clave").toString()));
		}
		return usuarios;
	}
	
	public boolean autenticar(Usuario user){
		document=new BasicDBObject();
		document.put("usuario", user.getEmail());
		document.put("clave", user.getClave());
		DBCursor dc=mInstance.readUsuario(document);
		if(dc.count()==1)return true;
		else return false;
	}
	
	public boolean deleteUsuario(String user){
		document=new BasicDBObject();
		document.put("usuario", user);
		return mInstance.deleteUsuario(document);
	}

	public void desconectar(){
		mInstance.cerrarBD();
	}
	
	public void darAlta(){
		int i=0;
		document=new BasicDBObject();
		document.put("dadoAlta", "false");
		DBCursor dc= mInstance.readUsuario(document);
		while(dc.hasNext()){
			i++;
			DBObject document1=dc.next();
			DBObject document2=document1;
			document2.put("dadoAlta", "true");
			deleteUsuario(document1.get("usuario").toString());
			createUsuario(document2.get("usuario").toString(),document2.get("clave").toString(),document2.get("nombre").toString(), document2.get("apellidos").toString(),document2.get("rol").toString(), document2.get("direccion").toString(), document2.get("telefono").toString(),(boolean)document2.get("dadoAlta"));
		}
	}
	

	
	public void cambiarRol(String user, String rol){
		document=new BasicDBObject();
		document.put("usuario", user);
		DBCursor dc= mInstance.readUsuario(document);
		DBObject nuevo=dc.next();
		nuevo.put("rol", rol);
		mInstance.updateUsuario(document, nuevo);
	}
	
	public String recuperarClave(String email){
		document=new BasicDBObject();
		document.put("usuario", email);
		DBCursor dc= mInstance.readUsuario(document);
		DBObject o=dc.next();
		return o.get("clave").toString();
	}
	
	public void cambiarClave(String user, String clave){
		document=new BasicDBObject();
		document.put("usuario", user);
		DBCursor dc= mInstance.readUsuario(document);
		DBObject nuevo=dc.next();
		nuevo.put("clave", clave);
		mInstance.updateUsuario(document, nuevo);
	}

	public String getRol(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("rol").toString();
	  }
	
	public String getNombre(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("nombre").toString();
	  }
	
	public boolean getDadoAlta(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return (boolean) dc.next().get("dadoAlta");
	  }
	
	public String getApellidos(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("apellidos").toString();
	  }
	
	public String getDireccion(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return (String) dc.next().get("direccion");
	  }
	
	public String getTelefono(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("telefono").toString();
	  }
	
	public String getEmail(Usuario user){
	    document=new BasicDBObject();
	    document.put("usuario", user.getEmail());
	    document.put("clave", user.getClave());
	    DBCursor dc=mInstance.readUsuario(document);
	    return dc.next().get("email").toString();
	  }
	
	public LinkedList<String[]> crearTablaDadosAlta(LinkedList<Usuario> users) {
		LinkedList<String[]> listaUsuariosVista =new LinkedList<String[]> ();
		
		for (int i=0;i<users.size();i++){
			Usuario u = users.get(i);
			boolean dadoAlta = getDadoAlta(u);
			String[] datosUsuarios = new String[2];
			datosUsuarios[0] = u.getEmail();
			datosUsuarios[1] = getNombre(u);
			if(!dadoAlta) listaUsuariosVista.add(datosUsuarios);
		}
		return listaUsuariosVista;
	}
	
	public LinkedList<String[]> crearTablaDadosBaja(LinkedList<Usuario> users) {
		LinkedList<String[]> listaUsuariosVista =new LinkedList<String[]> ();
		
		for (int i=0;i<users.size();i++){
			Usuario u = users.get(i);
			boolean dadoAlta = getDadoAlta(u);
			String[] datosUsuarios = new String[2];
			datosUsuarios[0] = u.getEmail();
			datosUsuarios[1] = getNombre(u);
			if(dadoAlta) listaUsuariosVista.add(datosUsuarios);
		}
		return listaUsuariosVista;
	}
	
	public LinkedList<String[]> crearTablaRoles(LinkedList<Usuario> users) {
				
		
		LinkedList<String[]> listaUsuariosVistaRol =new LinkedList<String[]>();
		
		
		for (int i=0;i<users.size();i++){
			Usuario u = users.get(i);
			boolean dadoAlta = getDadoAlta(u);
			String[] rolesUsuarios = new String[2];
			rolesUsuarios[0] = u.getEmail();
			rolesUsuarios[1] = getRol(u);
			if(dadoAlta) listaUsuariosVistaRol.add(rolesUsuarios);
		}
		
		
		return listaUsuariosVistaRol;
	}
	
	public LinkedList<String> crearTablaUsuarios(LinkedList<Usuario> users) {
	    LinkedList<String> listaUsuariosVista =new LinkedList<String> ();
	    
	    for (int i=0;i<users.size();i++){
	      Usuario u = users.get(i);
	      String datosUsuarios = u.getEmail();
	      listaUsuariosVista.add(datosUsuarios);
	    }
	    return listaUsuariosVista;
	  }
	
	public void darDeAlta(String email){
	    document=new BasicDBObject();
	    document.put("usuario", email);
	    DBCursor dc=mInstance.readUsuario(document);
	    DBObject antiguo=dc.next();
	    DBObject nuevo=antiguo;
	    nuevo.put("dadoAlta", true);
	    mInstance.updateUsuario(document, nuevo);
	  }
	
	public void darDeBaja(String email){
	    document=new BasicDBObject();
	    document.put("usuario", email);
	    DBCursor dc=mInstance.readUsuario(document);
	    DBObject antiguo=dc.next();
	    DBObject nuevo=antiguo;
	    nuevo.put("dadoAlta", false);
	    mInstance.updateUsuario(document, nuevo);
	  }
}
