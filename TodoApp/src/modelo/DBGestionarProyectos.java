package modelo;

import java.util.LinkedList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class DBGestionarProyectos {
	
	DataBase mInstance;
	
	public DBGestionarProyectos(){
		mInstance=DataBase.getInstance();
	}
	
	public boolean crearProyecto(String email, String nProyecto){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		document.put("nProyecto", nProyecto);
		return mInstance.createProyecto(document);
	}
	
	public int nProyectos(String email){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		return mInstance.readProyecto(document).size();
	}
	
	public void borrarProyecto(String email){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		mInstance.deleteProyecto(document);
	}
	
	public void deleteProyecto(String email, String nombre){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		document.put("nProyecto", nombre);
		mInstance.deleteProyecto(document);
	}
	
	public LinkedList<String> leerProyectos(String email){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		LinkedList<DBObject>proyectos=mInstance.readProyecto(document);
		LinkedList<String>nombreProyectos=new LinkedList<String>();
		for(int i=0; i<proyectos.size(); i++){
			nombreProyectos.add(proyectos.get(i).get("nProyecto").toString());
		}
		return nombreProyectos;
	}
	
	public boolean leerProyecto(String email, String nombre){
		DBObject document=new BasicDBObject();
		document.put("email", email);
		document.put("nProyecto", nombre);
		if(mInstance.readProyecto(document).size()==1) return true;
		else return false;
		
	}
	
}
