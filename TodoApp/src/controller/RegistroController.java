package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Usuario;


@Controller
public class RegistroController extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@RequestMapping("registrar")
	public ModelAndView registro(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		return new ModelAndView("/WEB-INF/Vistas/Registrar.jsp");
	}
	
	@RequestMapping("reg")
	public ModelAndView registrar(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		boolean existe=false;
		
		String user=request.getParameter("userEmailReg");
		String pass=request.getParameter("contrasenaReg");
		String pass2=request.getParameter("contrasenaReg2");
		
		
		String mensaje="";
		if(!user.equals("") && !pass.equals("")){
			while(!users.isEmpty()){
				if(users.poll().getEmail().equals(user)){
					existe=true;
					mensaje="El usuario ya existe";
				}
			}
			if(!existe){
				if(pass.equals(pass2)){
					if(pass.length()>=8){
						usuarios.createUsuario(user, pass, "John", "Doe", "Usuario", "Calle sn", "000000000", false);
						mensaje="Usuario registrado correctamente.";
					}
					else{
						mensaje="La longitud m�nima de la contrase�a debe ser de 8 caracteres";
						return new ModelAndView("/WEB-INF/Vistas/Registrar.jsp", "mensaje",mensaje);
					}
					
				}
				else{
					mensaje="Las password no son iguales";
					return new ModelAndView("/WEB-INF/Vistas/Registrar.jsp", "mensaje",mensaje);
				}
			}
		}else{
			mensaje="Introduzca el email y la contrase�a";
			return new ModelAndView("/WEB-INF/Vistas/Registrar.jsp", "mensaje",mensaje);
		}
		return new ModelAndView("/index.jsp", "mensaje",mensaje);
		
	}
	
	@RequestMapping("entrar")
	public ModelAndView prueba(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException, InstantiationException, IllegalAccessException{
		String mensaje;
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		ModelAndView model;
		PrincipalUsuarioController puc = new PrincipalUsuarioController();
		
		
		dl.setEmailLogin(request.getParameter("userEmailLogin"));
		//usuarios.setEmailLogin(request.getParameter("userEmailLogin"));
		
		Usuario u = new Usuario(request.getParameter("userEmailLogin"), request.getParameter("contrasenaLogin"));
		boolean autenticado = usuarios.autenticar(u);
		if(autenticado){
			String emailLogin = dl.getEmailLogin();
			String rol = usuarios.getRol(u);
			if(rol.equals("Administrador")){
				if(usuarios.getDadoAlta(u)){
					model = new ModelAndView("/WEB-INF/Vistas/Vista_PrincipalAdmin.jsp");
					model.addObject("emailLogueado", emailLogin);
				}
				else{
					mensaje = "No est� dado de alta. Espere a que un administrador le de de alta en el sistema.";
					model = new ModelAndView("/index.jsp", "mensaje", mensaje);
				}
			}
			else{
				if(usuarios.getDadoAlta(u)){
					model = new ModelAndView("/WEB-INF/Vistas/vista_pantallaPrincipalUsuarios.jsp");
					LinkedList<String[]> listaTareasCompletas = puc.cargarTareasCompletas(emailLogin);
					LinkedList<String[]> listaTareasActuales = puc.cargarTareasActuales(emailLogin);
					LinkedList<String[]> listaTareasFuturas = puc.cargarTareasFuturas(emailLogin);
					
					model.addObject("tareasCompletas",listaTareasCompletas);
					model.addObject("tareasActuales",listaTareasActuales);
					model.addObject("tareasFuturas",listaTareasFuturas);
				}
				else{
					mensaje = "No est� dado de alta. Espere a que un administrador le de de alta en el sistema.";
					model = new ModelAndView("/index.jsp", "mensaje", mensaje);
				}
			}
		}
		else{
			mensaje = "El usuario/contrase�a es err�neo";
			model = new ModelAndView("/index.jsp", "mensaje", mensaje);
		}
		return model;
	}
}
