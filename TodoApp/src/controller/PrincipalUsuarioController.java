package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarProyectos;
import modelo.DBGestionarTarea;
import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Tarea;
import modelo.Usuario;

@Controller
public class PrincipalUsuarioController {
	@RequestMapping("creaProyecto")
	public ModelAndView creaProyecto(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/crearProyecto.jsp");
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		DatosLogin dl = new DatosLogin();
		LinkedList<String>proyectos=gestProy.leerProyectos(dl.getEmailLogin());
		model.addObject("proyectos", proyectos);
		return model;
	}
	
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadas(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActuales(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturas(tasks,email);
		
		return listaTareas;
	}
	
	@RequestMapping("principalUser")
	public ModelAndView principal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/vista_pantallaPrincipalUsuarios.jsp");
		DatosLogin dl = new DatosLogin();
		
		LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(dl.getEmailLogin());
		LinkedList<String[]> listaTareasActuales = cargarTareasActuales(dl.getEmailLogin());
		LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(dl.getEmailLogin());
		
		model.addObject("tareasCompletas",listaTareasCompletas);
		model.addObject("tareasActuales",listaTareasActuales);
		model.addObject("tareasFuturas",listaTareasFuturas);
		return model;
	}
	
	@RequestMapping("crearTarea")
	public ModelAndView creaTarea(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/CrearTarea.jsp");
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		DatosLogin dl = new DatosLogin();
		LinkedList<String>proyectos=gestProy.leerProyectos(dl.getEmailLogin());
		model.addObject("proyectos", proyectos);
		return model;
	}
	
	@RequestMapping("cambiarContraseņa")
	public ModelAndView cambiacontra(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/CambiarContraseņa.jsp");
		return model;
	}
	
	@RequestMapping("verPerfilo")
	public ModelAndView verPerfilU(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/MiPerfilUsuario.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("emailUser", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		return model;
	}
	
	@RequestMapping("cerrarSesionUser")
	public ModelAndView cerrar(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		/*Codigo para desconectarse (Deslogueo)*/
		return model;
	}
	
	/**Con filtros**/
	public LinkedList<String[]> cargarTareasCompletasFiltradas(String email, String filtro){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks = new LinkedList<Tarea>();
		
		if(filtro.equals("fecha")) tasks = tareas.readTareasOrdenadasPorFecha();
		else if(filtro.equals("proyecto")) tasks = tareas.readTareasOrdenadasPorProyecto();
		else if(filtro.equals("prioridad")) tasks = tareas.readTareasOrdenadasPorPrioridad();
		
		LinkedList<String[]> listaTareas = tareas.crearTablaTareasCompletadas(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActualesFiltradas(String email, String filtro){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks = new LinkedList<Tarea>();
		if(filtro.equals("fecha")) tasks = tareas.readTareasOrdenadasPorFecha();
		else if(filtro.equals("proyecto")) tasks = tareas.readTareasOrdenadasPorProyecto();
		else if(filtro.equals("prioridad")) tasks = tareas.readTareasOrdenadasPorPrioridad();
		LinkedList<String[]> listaTareas = tareas.crearTablaTareasActuales(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturasFiltradas(String email, String filtro){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks = new LinkedList<Tarea>();
		if(filtro.equals("fecha")) tasks = tareas.readTareasOrdenadasPorFecha();
		else if(filtro.equals("proyecto")) tasks = tareas.readTareasOrdenadasPorProyecto();
		else if(filtro.equals("prioridad")) tasks = tareas.readTareasOrdenadasPorPrioridad();
		LinkedList<String[]> listaTareas = tareas.crearTablaTareasFuturas(tasks,email);
		
		return listaTareas;
	}
	
	@RequestMapping("filtrarTablas")
	public ModelAndView filtrarTablas(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/vista_pantallaPrincipalUsuarios.jsp");
		
		DatosLogin dl = new DatosLogin();
		LinkedList<String[]> listaTareasCompletas = new LinkedList<String[]>();
		LinkedList<String[]> listaTareasActuales = new LinkedList<String[]>();
		LinkedList<String[]> listaTareasFuturas = new LinkedList<String[]>();
		boolean botonFecha = request.getParameter("filtroFechaBoton") != null ;
		boolean botonProyecto = request.getParameter("filtroProyectoBoton") != null ;
		boolean botonPrioridad = request.getParameter("filtroPrioridadBoton") != null ;
		
		if(botonFecha){
			String filtro = "fecha";
			listaTareasCompletas = cargarTareasCompletasFiltradas(dl.getEmailLogin(),filtro);
			listaTareasActuales = cargarTareasActualesFiltradas(dl.getEmailLogin(),filtro);
			listaTareasFuturas = cargarTareasFuturasFiltradas(dl.getEmailLogin(),filtro);
			
			model.addObject("tareasCompletas",listaTareasCompletas);
			model.addObject("tareasActuales",listaTareasActuales);
			model.addObject("tareasFuturas",listaTareasFuturas);
		}
		
		if(botonProyecto){
			String filtro = "proyecto";
			listaTareasCompletas = cargarTareasCompletasFiltradas(dl.getEmailLogin(),filtro);
			listaTareasActuales = cargarTareasActualesFiltradas(dl.getEmailLogin(),filtro);
			listaTareasFuturas = cargarTareasFuturasFiltradas(dl.getEmailLogin(),filtro);
			
			model.addObject("tareasCompletas",listaTareasCompletas);
			model.addObject("tareasActuales",listaTareasActuales);
			model.addObject("tareasFuturas",listaTareasFuturas);
		}
		
		if(botonPrioridad){
			String filtro = "prioridad";
			listaTareasCompletas = cargarTareasCompletasFiltradas(dl.getEmailLogin(),filtro);
			listaTareasActuales = cargarTareasActualesFiltradas(dl.getEmailLogin(),filtro);
			listaTareasFuturas = cargarTareasFuturasFiltradas(dl.getEmailLogin(),filtro);
			
			model.addObject("tareasCompletas",listaTareasCompletas);
			model.addObject("tareasActuales",listaTareasActuales);
			model.addObject("tareasFuturas",listaTareasFuturas);
		}
		
		
		return model;
	}
}
