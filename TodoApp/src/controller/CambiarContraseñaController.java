package controller;

	import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import org.springframework.stereotype.Controller;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Usuario;
	
	@Controller
	public class CambiarContraseñaController {
		
		
		@RequestMapping("InterfazCambiarContrasena")
		public ModelAndView darModelo(){
			ModelAndView model = new ModelAndView("/WEB-INF/Vistas/CambiarContraseña.jsp");
			return model;
		}
		
		
		@RequestMapping("Cambiar_Contrasena")
		public ModelAndView cambiarContraseña(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
				
			String contrasenaVieja = request.getParameter("contrasenaActual");
			String nuevaContrasena = request.getParameter("contrasenaNueva");
			String repNuevaContrasena = request.getParameter("contrasenaNuevaRep");
			
			String feedback="";
			
			DBGestionarUsuario instancia = new DBGestionarUsuario();
			DatosLogin dl = new DatosLogin();
			String email = dl.getEmailLogin();
			
			if(contrasenaVieja.isEmpty() || nuevaContrasena.isEmpty() || repNuevaContrasena.isEmpty()) feedback="Ninguno de los tres campos puede estar vacío";
			else{
				if(contrasenaVieja.equals(email)){
					if(nuevaContrasena.equals(repNuevaContrasena)){
						LinkedList<Usuario> lista = instancia.readUsuarios();
						
						for(int i=0; i<lista.size();i++){
							if(lista.get(i).getEmail().equals(email) && lista.get(i).getClave().equals(contrasenaVieja)){
								Usuario u = new Usuario(email,contrasenaVieja);
								instancia.getNombre(u);
								instancia.updateUsuario(email, nuevaContrasena, instancia.getNombre(u), instancia.getApellidos(u), instancia.getRol(u), instancia.getDireccion(u), instancia.getTelefono(u));
								feedback="Contraseña cambiada con éxito";
							}
						}
					}
					else feedback="Las contraseñas deben coincidir";
				}
				else feedback="La contraseña actual no coincide";
				
			}

			
			
			return new ModelAndView("/WEB-INF/Vistas/CambiarContraseña.jsp","feedback",feedback);
		}
		
	
}
