package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarProyectos;
import modelo.DBGestionarTarea;
import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Tarea;
import modelo.Usuario;

@Controller

public class CrearTareaController {
	
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadas(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActuales(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturas(tasks,email);
		
		return listaTareas;
	}
	
	@RequestMapping("homeUser")
	public ModelAndView principal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/vista_pantallaPrincipalUsuarios.jsp");
		DatosLogin dl = new DatosLogin();
		
		LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(dl.getEmailLogin());
		LinkedList<String[]> listaTareasActuales = cargarTareasActuales(dl.getEmailLogin());
		LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(dl.getEmailLogin());
		
		model.addObject("tareasCompletas",listaTareasCompletas);
		model.addObject("tareasActuales",listaTareasActuales);
		model.addObject("tareasFuturas",listaTareasFuturas);
		return model;
	}
	
	@RequestMapping("crearProyecto")
	public ModelAndView creaProyecto(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/crearProyecto.jsp");
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		DatosLogin dl = new DatosLogin();
		LinkedList<String>proyectos=gestProy.leerProyectos(dl.getEmailLogin());
		model.addObject("proyectos", proyectos);
		return model;
	}
	
	@RequestMapping("cambiarPass")
	public ModelAndView cambiacontra(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/CambiarContraseņa.jsp");
		return model;
	}
	
	@RequestMapping("verPerfilUsuario")
	public ModelAndView verPerfilU(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/MiPerfilUsuario.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("emailUser", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		return model;
	}
	
	@RequestMapping("logoutTodoApp")
	public ModelAndView cerrar(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		/*Codigo para desconectarse (Deslogueo)*/
		return model;
	}
	
	@RequestMapping("tareaNueva")
	public ModelAndView crearTareaNueva(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/CrearTarea.jsp");

		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks = tareas.readTareas();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		String nombre = request.getParameter("nombreCT");
		String prioridad = request.getParameter("prioridadCT");
		String fechaInicio = request.getParameter("fechaInicioCT");
		String fechaFin = request.getParameter("fechaFinCT");
		String proyecto = request.getParameter("proyectoCT");
		String notas = request.getParameter("notasCT");
		boolean completada = request.getParameter("completadaCrearTarea") != null;
		boolean existe = false;
		String mensaje="";
		
		while(!tasks.isEmpty()){
			if((tasks.peek().getEmail().equals(email))&&(tasks.peek().getnTarea().equals(nombre))){
				existe=true;
				mensaje="La tarea ya existe";

			}
			tasks.poll();
		}
		
		if(!existe){
			tareas.createTarea(email, nombre, prioridad, proyecto, completada, fechaInicio, fechaFin, notas);
			mensaje="Tarea creada correctamente";

		}
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		LinkedList<String>proyectos=gestProy.leerProyectos(dl.getEmailLogin());
		model.addObject("proyectos", proyectos);
		model.addObject("feedback", mensaje);

		return model;
		
		
	}

}
