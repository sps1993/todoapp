package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Usuario;
@Controller
public class ListarUsuarioController {

	@RequestMapping("principaladministrador")
	public ModelAndView menuPrincipal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/Vista_PrincipalAdmin.jsp");
		return model;
	}
	
	@RequestMapping("altasusuariosadmin")
	public ModelAndView darDeAlta(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/darDeAlta.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
		model.addObject("lista", listaUsuariosVista);
		return model;
	}
	
	@RequestMapping("bajasusuarios")
	public ModelAndView darDeBaja(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/darDeBaja.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
		model.addObject("lista", listaUsuariosVista);
		
		return model;
	}
	
	@RequestMapping("modificarroles")
	public ModelAndView cambiarRol(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		LinkedList<String[]> listaUsuariosVistaRol =usuarios.crearTablaRoles(users);
		LinkedList<String> listaCheckBox = new LinkedList<>();
		String rol=request.getParameter("rol");
		
		
		for (int i=0;i<listaUsuariosVistaRol.size();i++){
			String nombreCheckbox="checkbox"+i;
			boolean check = request.getParameter(nombreCheckbox) != null;
			String checkbox = request.getParameter(nombreCheckbox);
			if(check) listaCheckBox.add(checkbox+i);
		}
		
		
		for(int k=0; k<listaCheckBox.size();k++){
			String checkBoxActual=listaCheckBox.get(k);
			String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
			int numeroFila = Integer.parseInt(filaTabla);
			String []datosUsuario=listaUsuariosVistaRol.get(numeroFila);
			usuarios.cambiarRol(datosUsuario[0], rol);
		}
		
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/Vista_CambiarRoles.jsp");
		users =usuarios.readUsuarios();
		listaUsuariosVistaRol =usuarios.crearTablaRoles(users);
		model.addObject("lista", listaUsuariosVistaRol);
		return model;
		
	}
	
	@RequestMapping("verAdminPerfil")
	public ModelAndView verPerfilAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/MiPerfil.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();

		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){

			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("email", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		return model;
		
	}
	
	@RequestMapping("PantallaPrincipalTodoApp")
	public ModelAndView cerrarSesion(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		/*Codigo para desconectarse (Deslogueo)*/
		return model;
	}
	
	@RequestMapping("ver")
	public ModelAndView listarUsuarios(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/MiPerfil.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		boolean userEncontrado=false;
		LinkedList<String> listaUsuariosVista =usuarios.crearTablaUsuarios(users);
		
		for(int i=0; i<listaUsuariosVista.size(); i++){
			String userEmail = listaUsuariosVista.get(i);
			Usuario u=users.get(i);
			
			if(u.getEmail().equals(userEmail)) userEncontrado=true;
			
			boolean nombreBoton=request.getParameter("boton"+i) != null;
			
			if (nombreBoton && userEncontrado){
				model.addObject("nombre", u.getEmail());
				model.addObject("apellidos", usuarios.getApellidos(u));
				model.addObject("direccion", usuarios.getDireccion(u));
				model.addObject("telefono", usuarios.getTelefono(u));
			}
			
		}
		
		return model;
		
	}
	
	@RequestMapping("verTareas")
	public ModelAndView listarTareas(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/tareasUsuariosAdmin.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		boolean userEncontrado=false;
		LinkedList<String> listaUsuariosVista =usuarios.crearTablaUsuarios(users);
		
		for(int i=0; i<listaUsuariosVista.size(); i++){
			String userEmail = listaUsuariosVista.get(i);
			Usuario u=users.get(i);
			
			if(u.getEmail().equals(userEmail)) userEncontrado=true;
			
			boolean nombreBoton=request.getParameter("boton"+i) != null;
			
			if (nombreBoton && userEncontrado){
				PrincipalUsuarioController puc= new PrincipalUsuarioController();
				LinkedList<String[]> listaTareasCompletas = puc.cargarTareasCompletas(userEmail);
				LinkedList<String[]> listaTareasActuales = puc.cargarTareasActuales(userEmail);
				LinkedList<String[]> listaTareasFuturas = puc.cargarTareasFuturas(userEmail);
				model.addObject("tareasCompletas",listaTareasCompletas);
				model.addObject("tareasActuales",listaTareasActuales);
				model.addObject("tareasFuturas",listaTareasFuturas);
			}
			
		}
		return model;	
	}
	
}
