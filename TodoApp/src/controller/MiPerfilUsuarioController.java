package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarProyectos;
import modelo.DBGestionarTarea;
import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Usuario;

@Controller

public class MiPerfilUsuarioController {
	@RequestMapping("modificarPerfil")
	public ModelAndView accesoModificar(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/ModificarPerfilUsuario.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("email", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);

		return model;
	}
	
	@RequestMapping("eliminarCuenta")
	public ModelAndView borrarCuenta(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		DBGestionarTarea gt = new DBGestionarTarea();
		DBGestionarProyectos gp = new DBGestionarProyectos();
		DBGestionarUsuario gu = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		
		gt.borrarTareas(dl.getEmailLogin());
		gp.borrarProyecto(dl.getEmailLogin());
		gu.deleteUsuario(dl.getEmailLogin());
		return model;
	}

}
