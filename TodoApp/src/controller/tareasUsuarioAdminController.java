package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarTarea;
import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Tarea;
import modelo.Usuario;

@Controller
public class tareasUsuarioAdminController {
	@RequestMapping("PrincipalTUA")
	public ModelAndView menuPrincipal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/Vista_PrincipalAdmin.jsp");
		return model;
	}
	
	@RequestMapping("darAltasTUA")
	public ModelAndView darDeAlta(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/darDeAlta.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
		model.addObject("lista", listaUsuariosVista);
		return model;
	}
	
	@RequestMapping("darBajasTUA")
	public ModelAndView darDeBaja(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/darDeBaja.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
		model.addObject("lista", listaUsuariosVista);
		
		return model;
	}
	
	@RequestMapping("cambiarRolesTUA")
	public ModelAndView cambiarRoles(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/Vista_CambiarRoles.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVistaRol =usuarios.crearTablaRoles(users);
		model.addObject("lista", listaUsuariosVistaRol);
		return model;
	}
	
	@RequestMapping("verPerfilAdminTUA")
	public ModelAndView verPerfilAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/MiPerfil.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("email", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		return model;
		
	}
	
	@RequestMapping("indexTUA")
	public ModelAndView cerrarSesion(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		/*Codigo para desconectarse (Deslogueo)*/
		return model;
	}
	
	@RequestMapping("verInfoTareaAdmin")
	public ModelAndView verInfoTareaAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/InformacionTareaAdmin.jsp");
		DBGestionarTarea tareas = new DBGestionarTarea();
		boolean tareaEncontrada = false;
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Tarea> tasks = new LinkedList<Tarea>();
		tasks = tareas.readTareas();
		LinkedList<String[]> listaTareasVista = new LinkedList<String[]>();
		listaTareasVista.addAll(tareas.crearTablaTareasActuales(tasks, email));
		listaTareasVista.addAll(tareas.crearTablaTareasCompletadas(tasks, email));
		listaTareasVista.addAll(tareas.crearTablaTareasFuturas(tasks, email));
		tasks = tareas.readTareas();
		
		for(int i=0; i<listaTareasVista.size(); i++){
			String tar = listaTareasVista.get(i)[0];
			Tarea t=tasks.get(i);
			if((t.getnTarea().equals(tar))&&(t.getEmail().equals(email)))
				tareaEncontrada = true;
			
			boolean nombreBoton = request.getParameter("boton"+i) != null;
			if (nombreBoton && tareaEncontrada){
				model.addObject("nombreTar", t.getnTarea());
				model.addObject("prioridadTar", t.getPrioridad());
				model.addObject("proyectoTar", t.getProyecto());
				model.addObject("completadaInformacionTarea", t.isCompletada());
				model.addObject("fInicioTar", t.getFechaInicio());
				model.addObject("fLimiteTar", t.getFechaFin());
				model.addObject("getNotasTar", t.getNotas());
			}
			
		}
		
		return model;
	}
}
