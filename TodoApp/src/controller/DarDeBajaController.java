package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Usuario;

@Controller
public class DarDeBajaController {
	
	@RequestMapping("PrincipalAdmin")
	public ModelAndView menuPrincipal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/Vista_PrincipalAdmin.jsp");
		return model;
	}
	
	@RequestMapping("AltasUsuarios")
	public ModelAndView darDeAlta(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/darDeAlta.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosAlta(users);
		model.addObject("lista", listaUsuariosVista);
		return model;
	}
	
	@RequestMapping("Dar_Bajas")
	public ModelAndView darDeBaja(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		LinkedList<String[]> listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
		LinkedList<String> listaCheckBox = new LinkedList<>();
		
		for (int i=0;i<listaUsuariosVista.size();i++){
			String nombreCheckbox="checkbox"+i;

			boolean check = request.getParameter(nombreCheckbox) != null;
			String checkbox = request.getParameter(nombreCheckbox);

			if(check) listaCheckBox.add(checkbox+i);
		}
		
		for(int k=0; k<listaCheckBox.size();k++){
			String checkBoxActual=listaCheckBox.get(k);

			String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
			int numeroFila = Integer.parseInt(filaTabla);

				String []datosUsuario=listaUsuariosVista.get(numeroFila);

				usuarios.darDeBaja(datosUsuario[0]);
		}
		
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/darDeBaja.jsp");
		users =usuarios.readUsuarios();
		listaUsuariosVista =usuarios.crearTablaDadosBaja(users);
		model.addObject("lista", listaUsuariosVista);
		return model;
	}
	
	@RequestMapping("cambioRoles")
	public ModelAndView cambiarRoles(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{

		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/Vista_CambiarRoles.jsp");
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String[]> listaUsuariosVistaRol =usuarios.crearTablaRoles(users);
		model.addObject("lista", listaUsuariosVistaRol);
		return model;
	}
	
	@RequestMapping("listarusers")
	public ModelAndView listarUsuarios(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		LinkedList<Usuario> users =usuarios.readUsuarios();
		
		LinkedList<String> listaUsuariosVista =usuarios.crearTablaUsuarios(users);
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/ListarUsuarios.jsp");
		model.addObject("listaTodos", listaUsuariosVista);
		return model;
	}
	
	@RequestMapping("vermiperfil")
	public ModelAndView verPerfilAdmin(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/MiPerfil.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("email", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		return model;
		
	}
	
	@RequestMapping("login")
	public ModelAndView cerrarSesion(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		/*Codigo para desconectarse (Deslogueo)*/
		return model;
	}
	
}