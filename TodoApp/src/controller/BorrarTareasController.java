package controller;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarTarea;
import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Tarea;
import modelo.Usuario;

@Controller
public class BorrarTareasController {
	
	@RequestMapping("borrarTarea")
	public ModelAndView borrar(HttpServletRequest request, HttpServletResponse response){
		DBGestionarTarea tareas = new DBGestionarTarea();
		DatosLogin dl = new DatosLogin();
		LinkedList<Tarea> ListaTareas =tareas.readTareas();
		LinkedList<String> listaTareasVista = new LinkedList();	
		LinkedList<String> listaCheckBox = new LinkedList<>();
		
		//Agregamos los campos necesarios para identificar las tareas.
		for(int h =0; h< ListaTareas.size(); h++){
			listaTareasVista.add(ListaTareas.remove().getnTarea());
		}
		
		for (int i=0;i<listaTareasVista.size();i++){
			String nombreCheckbox="checkbox"+i;
			boolean check = request.getParameter(nombreCheckbox) != null;
			String checkbox = request.getParameter(nombreCheckbox);
			if(check) listaCheckBox.add(checkbox+i);
		}
		
		for(int k=0; k<listaCheckBox.size();k++){
			String checkBoxActual=listaCheckBox.get(k);
			String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
			int numeroFila = Integer.parseInt(filaTabla);
				String nombre=listaTareasVista.get(numeroFila);
				//Borramos tarea.
				tareas.deleteTarea(dl.getEmailLogin(), nombre);
		}
		
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/vista_pantallaPrincipalUsuarios.jsp");
		
		LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(dl.getEmailLogin());
		LinkedList<String[]> listaTareasActuales = cargarTareasActuales(dl.getEmailLogin());
		LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(dl.getEmailLogin());
		
		model.addObject("tareasCompletas",listaTareasCompletas);
		model.addObject("tareasActuales",listaTareasActuales);
		model.addObject("tareasFuturas",listaTareasFuturas);
		
		return model;
	}
	
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadas(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActuales(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturas(tasks,email);
		
		return listaTareas;
	}
}
