package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarUsuario;
import modelo.Usuario;

@Controller
public class RecuperarContrasenaController {
	
	@RequestMapping("/InterfazRecuperarContrasena")
	public ModelAndView darModelo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/Vista_RecuperarContrasena.jsp");
		return model;
	}
	
	@RequestMapping("/RecuperarContrase�a")
	public ModelAndView recuperar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ModelAndView model; 
		
		String mensaje=" ";
		String email = request.getParameter("inputEmail");
		boolean continuar=true;
		DBGestionarUsuario instancia = new DBGestionarUsuario();
		Usuario user;
		//Obtenemos el usuario de la contraseña que se quiere cambiar.
		LinkedList<Usuario> lista = instancia.readUsuarios();
		
	
		while(!lista.isEmpty() && continuar){
			Usuario aux = lista.removeFirst();
			if(aux.getEmail().equals(email)){
				mensaje="Clave recuperada: "+aux.getClave();
				continuar=false;
			}
			
		}
		return new ModelAndView("/WEB-INF/Vistas/Vista_RecuperarContrasena.jsp","mensaje",mensaje);
				
	}
	
	@RequestMapping("menuprincipal_home")
	public ModelAndView volverMenuprincipal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/index.jsp");
		return model;
	}
}
