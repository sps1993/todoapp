package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarTarea;
import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.DatosTarea;
import modelo.Tarea;
import modelo.Usuario;

@Controller

public class InformacionTareaController {
	
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadas(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActuales(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturas(tasks,email);
		
		return listaTareas;
	}
	
	@RequestMapping("volverTareasUsuario")
	public ModelAndView principal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/vista_pantallaPrincipalUsuarios.jsp");
		DatosLogin dl = new DatosLogin();
		DBGestionarTarea gt = new DBGestionarTarea();
		DatosTarea dt= new DatosTarea();
		boolean completada = request.getParameter("completadaInformacionTarea") != null;
			String nombreTarea = dt.getNombreTarea();
			String email = dl.getEmailLogin();
			
			DBGestionarTarea tareas = new DBGestionarTarea();
			LinkedList<Tarea> tasks = new LinkedList<Tarea>();
			tasks = tareas.readTareas();
			
			for(int i=0;i<tasks.size();i++){
				if( (tasks.get(i).getnTarea().equals(nombreTarea) ) && (tasks.get(i).getEmail().equals(email) ) ){
					gt.updateTarea(tasks.get(i).getEmail(), tasks.get(i).getnTarea(), tasks.get(i).getPrioridad(), tasks.get(i).getProyecto(), completada, tasks.get(i).getFechaInicio(), tasks.get(i).getFechaFin(), tasks.get(i).getNotas());
				}
			}
		
		LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(dl.getEmailLogin());
		LinkedList<String[]> listaTareasActuales = cargarTareasActuales(dl.getEmailLogin());
		LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(dl.getEmailLogin());
		
		model.addObject("tareasCompletas",listaTareasCompletas);
		model.addObject("tareasActuales",listaTareasActuales);
		model.addObject("tareasFuturas",listaTareasFuturas);
		return model;
	}
	
	@RequestMapping("verInfoTarea")
	public ModelAndView infoTarea(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model;
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		DBGestionarTarea tareas = new DBGestionarTarea();
		DatosTarea dt = new DatosTarea();
		LinkedList<String[]> listaTareasVista = new LinkedList<String[]>();
		
		LinkedList<String[]> listaTareasVistaCompletadas= new LinkedList<String[]>();
		LinkedList<String[]> listaTareasVistaActuales = new LinkedList<String[]>();
		LinkedList<String[]> listaTareasVistaFuturas = new LinkedList<String[]>();
		
		LinkedList<Tarea> tasks = new LinkedList<Tarea>();
		tasks = tareas.readTareas();
		listaTareasVista.addAll(tareas.crearTablaTareasCompletadas(tasks, email));
		tasks = tareas.readTareas();
		listaTareasVista.addAll(tareas.crearTablaTareasActuales(tasks, email));
		tasks = tareas.readTareas();
		listaTareasVista.addAll(tareas.crearTablaTareasFuturas(tasks, email));
		tasks = tareas.readTareas();
		
		//LinkedList<String> listaCheckBox = new LinkedList<>();
		LinkedList<String> listaCheckBoxCompletadas = new LinkedList<>();
		LinkedList<String> listaCheckBoxActuales = new LinkedList<>();
		LinkedList<String> listaCheckBoxFuturas = new LinkedList<>();
		boolean botonBorrar = request.getParameter("borrarPrincipalUsuarios") != null;
	
		if(botonBorrar){
			listaTareasVistaCompletadas.addAll(tareas.crearTablaTareasCompletadas(tasks, email));
			tasks = tareas.readTareas();
			listaTareasVistaActuales.addAll(tareas.crearTablaTareasActuales(tasks, email));
			tasks = tareas.readTareas();
			listaTareasVistaFuturas.addAll(tareas.crearTablaTareasFuturas(tasks, email));
			tasks = tareas.readTareas();
			
			
			for (int i=0;i<listaTareasVistaCompletadas.size();i++){
				String nombreCheckbox="checkbox"+i+"tareasCompletadas";
				boolean check = request.getParameter(nombreCheckbox) != null;
				String checkbox = request.getParameter(nombreCheckbox);
				if(check) listaCheckBoxCompletadas.add(checkbox+i);
			}
			
			for (int i=0;i<listaTareasVistaActuales.size();i++){
				String nombreCheckbox="checkbox"+i+"tareasActuales";
				boolean check = request.getParameter(nombreCheckbox) != null;
				String checkbox = request.getParameter(nombreCheckbox);
				if(check) listaCheckBoxActuales.add(checkbox+i);
			}
			
			for (int i=0;i<listaTareasVistaFuturas.size();i++){
				String nombreCheckbox="checkbox"+i+"tareasFuturas";
				boolean check = request.getParameter(nombreCheckbox) != null;
				String checkbox = request.getParameter(nombreCheckbox);
				if(check) listaCheckBoxFuturas.add(checkbox+i);
			}
			
			for(int k=0; k<listaCheckBoxCompletadas.size();k++){
				String checkBoxActual=listaCheckBoxCompletadas.get(k);
				String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
				//String filaTabla = checkBoxActual.substring(8, 9);
				int numeroFila = Integer.parseInt(filaTabla);
					String []datosTarea=listaTareasVistaCompletadas.get(numeroFila);
					tareas.deleteTarea(dl.getEmailLogin(), datosTarea[0]);
			}
			
			for(int k=0; k<listaCheckBoxActuales.size();k++){
				String checkBoxActual=listaCheckBoxActuales.get(k);
				String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
				//String filaTabla = checkBoxActual.substring(8, 9);

				int numeroFila = Integer.parseInt(filaTabla);

					String []datosTarea=listaTareasVistaActuales.get(numeroFila);

					tareas.deleteTarea(dl.getEmailLogin(), datosTarea[0]);
			}
			
			for(int k=0; k<listaCheckBoxFuturas.size();k++){
				String checkBoxActual=listaCheckBoxFuturas.get(k);

				String filaTabla = ""+checkBoxActual.charAt(checkBoxActual.length()-1);
				//String filaTabla = checkBoxActual.substring(8, 9);

				int numeroFila = Integer.parseInt(filaTabla);
					String []datosTarea=listaTareasVistaFuturas.get(numeroFila);
					tareas.deleteTarea(dl.getEmailLogin(), datosTarea[0]);
			}
			
			LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(dl.getEmailLogin());
			LinkedList<String[]> listaTareasActuales = cargarTareasActuales(dl.getEmailLogin());
			LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(dl.getEmailLogin());
			model=new ModelAndView("/WEB-INF/Vistas/vista_pantallaPrincipalUsuarios.jsp");
			model.addObject("tareasCompletas",listaTareasCompletas);
			model.addObject("tareasActuales",listaTareasActuales);
			model.addObject("tareasFuturas",listaTareasFuturas);
			
		}
		
		
		else{
			
			
			boolean nombreBotonCompletada = false;
			boolean nombreBotonActual = false;
			boolean nombreBotonFutura = false;
			int iteracion = 0;
			boolean tareaEncontrada=false;
			Tarea t = null;
			model = new ModelAndView("/WEB-INF/Vistas/InformacionTarea.jsp");
			
			for(int i=0; i<listaTareasVista.size(); i++){
				nombreBotonCompletada = request.getParameter("boton"+i+"tareasCompletadas") != null;
				nombreBotonActual = request.getParameter("boton"+i+"tareasActuales") != null;
				nombreBotonFutura = request.getParameter("boton"+i+"tareasFuturas") != null;
				if(nombreBotonCompletada==true || nombreBotonActual==true || nombreBotonFutura==true){
					iteracion=i;
					break;
				}
			}
			
			
			if(nombreBotonCompletada){
				//String nombreBoton=request.getParameter("boton"+iteracion+"tareasFuturas");
				LinkedList<String[]> listaTareasCompletadas = new LinkedList<String[]>();
				listaTareasCompletadas.addAll(tareas.crearTablaTareasCompletadas(tasks, email));
				tasks = tareas.readTareas();
				String tarCompletada = listaTareasCompletadas.get(iteracion)[0];
				for(int j=0;j<tasks.size();j++){
					t = tasks.get(j);
					if((t.getnTarea().equals(tarCompletada)) && (t.getEmail().equals(email))){
						tareaEncontrada=true;
						break;
					}
				}
				if(tareaEncontrada){
					dt.setNombreTarea(t.getnTarea());
					model.addObject("nombreTar", dt.getNombreTarea());
					model.addObject("prioridadTar", t.getPrioridad());
					model.addObject("proyectoTar", t.getProyecto());
					model.addObject("completadaInformacionTarea", t.isCompletada());
					model.addObject("fInicioTar", t.getFechaInicio());
					model.addObject("fLimiteTar", t.getFechaFin());
					model.addObject("getNotasTar", t.getNotas());
				}
			}
			
			if(nombreBotonActual){
				//String nombreBoton=request.getParameter("boton"+iteracion+"tareasFuturas");
				LinkedList<String[]> listaTareasActuales = new LinkedList<String[]>();
				listaTareasActuales.addAll(tareas.crearTablaTareasActuales(tasks, email));
				tasks = tareas.readTareas();
				String tarActual = listaTareasActuales.get(iteracion)[0];
				for(int j=0;j<tasks.size();j++){
					t = tasks.get(j);
					if((t.getnTarea().equals(tarActual)) && (t.getEmail().equals(email))){
						tareaEncontrada=true;
						break;
					}
				}
				
				if(tareaEncontrada){
					dt.setNombreTarea(t.getnTarea());
					model.addObject("nombreTar", dt.getNombreTarea());
					model.addObject("prioridadTar", t.getPrioridad());
					model.addObject("proyectoTar", t.getProyecto());
					model.addObject("completadaInformacionTarea", t.isCompletada());
					model.addObject("fInicioTar", t.getFechaInicio());
					model.addObject("fLimiteTar", t.getFechaFin());
					model.addObject("getNotasTar", t.getNotas());
				}
			}
			
			if(nombreBotonFutura){
				//String nombreBoton=request.getParameter("boton"+iteracion+"tareasFuturas");
				LinkedList<String[]> listaTareasFuturas = new LinkedList<String[]>();
				listaTareasFuturas.addAll(tareas.crearTablaTareasFuturas(tasks, email));
				tasks = tareas.readTareas();
				String tarFutura = listaTareasFuturas.get(iteracion)[0];
				for(int j=0;j<tasks.size();j++){
					t = tasks.get(j);
					if((t.getnTarea().equals(tarFutura)) && (t.getEmail().equals(email))){
						tareaEncontrada=true;
						break;
					}
				}
				
				if(tareaEncontrada){
					dt.setNombreTarea(t.getnTarea());
					model.addObject("nombreTar", dt.getNombreTarea());
					model.addObject("prioridadTar", t.getPrioridad());
					model.addObject("proyectoTar", t.getProyecto());
					model.addObject("completadaInformacionTarea", t.isCompletada());
					model.addObject("fInicioTar", t.getFechaInicio());
					model.addObject("fLimiteTar", t.getFechaFin());
					model.addObject("getNotasTar", t.getNotas());
				}
			}
	
			}
		return model;
		
	}

}
