package controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import modelo.DBGestionarProyectos;
import modelo.DBGestionarTarea;
import modelo.DBGestionarUsuario;
import modelo.DatosLogin;
import modelo.Tarea;
import modelo.Usuario;

@Controller
public class CrearProyectoController {
	
	public LinkedList<String[]> cargarTareasCompletas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasCompletadas(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasActuales(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasActuales(tasks,email);
		
		return listaTareas;
	}
	
	public LinkedList<String[]> cargarTareasFuturas(String email){
		DBGestionarTarea tareas = new DBGestionarTarea();
		LinkedList<Tarea> tasks =tareas.readTareas();
		LinkedList<String[]> listaTareas =tareas.crearTablaTareasFuturas(tasks,email);
		
		return listaTareas;
	}
	
	@RequestMapping("principalUsuario")
	public ModelAndView principal(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/vista_pantallaPrincipalUsuarios.jsp");
		DatosLogin dl = new DatosLogin();
		
		LinkedList<String[]> listaTareasCompletas = cargarTareasCompletas(dl.getEmailLogin());
		LinkedList<String[]> listaTareasActuales = cargarTareasActuales(dl.getEmailLogin());
		LinkedList<String[]> listaTareasFuturas = cargarTareasFuturas(dl.getEmailLogin());
		
		model.addObject("tareasCompletas",listaTareasCompletas);
		model.addObject("tareasActuales",listaTareasActuales);
		model.addObject("tareasFuturas",listaTareasFuturas);
		return model;
	}
	
	@RequestMapping("crearTareas")
	public ModelAndView creaTarea(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/CrearTarea.jsp");
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		DatosLogin dl = new DatosLogin();
		LinkedList<String>proyectos=gestProy.leerProyectos(dl.getEmailLogin());
		model.addObject("proyectos", proyectos);
		return model;
	}
	
	@RequestMapping("cambiarContrasena")
	public ModelAndView cambiacontra(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/CambiarContraseņa.jsp");
		return model;
	}
	
	@RequestMapping("verPerfilUser")
	public ModelAndView verPerfilU(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/MiPerfilUsuario.jsp");
		DBGestionarUsuario usuarios = new DBGestionarUsuario();
		DatosLogin dl = new DatosLogin();
		String email = dl.getEmailLogin();
		LinkedList<Usuario> users = usuarios.readUsuarios();
		int i=0;
		Usuario usuario;
		while(!(((usuario=(users.get(i))).getEmail()).equals(email))){
			i++;	
		}
		email = usuario.getEmail();
		String nombre = usuarios.getNombre(usuario);
		String apellidos = usuarios.getApellidos(usuario);
		String direccion = usuarios.getDireccion(usuario);
		String telefono = usuarios.getTelefono(usuario);
		model.addObject("nombre", nombre);
		model.addObject("emailUser", email);
		model.addObject("apellidos", apellidos);
		model.addObject("direccion", direccion);
		model.addObject("telefono", telefono);
		return model;
	}
	
	@RequestMapping("cerrarSesionUsuario")
	public ModelAndView cerrar(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("index.jsp");
		DBGestionarUsuario.getInstance().desconectar();
		
		/*Codigo para desconectarse (Deslogueo)*/
		return model;
	}
	
	@RequestMapping("crear")
	public ModelAndView creaProyecto(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/crearProyecto.jsp");
		DBGestionarProyectos gP=new DBGestionarProyectos();
		DatosLogin dl = new DatosLogin();
		gP.crearProyecto(dl.getEmailLogin(), request.getParameter("nuevoProyecto"));
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		LinkedList<String>proyectos=gestProy.leerProyectos(dl.getEmailLogin());
		String mensaje="Proyecto Creado";
		model.addObject("proyectos", proyectos);
		model.addObject("mensaje", mensaje);
		return model;
	}
	
	@RequestMapping("borrarProyecto")
	public ModelAndView borrarProyecto(HttpServletRequest request, HttpServletResponse response) 
		      throws ServletException, IOException{
		ModelAndView model = new ModelAndView("/WEB-INF/Vistas/crearProyecto.jsp");
		DBGestionarProyectos gP=new DBGestionarProyectos();
		DatosLogin dl = new DatosLogin();
		String nombreProyecto=request.getParameter("nombreProyectoBorrarCrearProyecto");
		boolean existe= gP.leerProyecto(dl.getEmailLogin(), nombreProyecto);
		if(existe){
			gP.deleteProyecto(dl.getEmailLogin(), nombreProyecto);
			DBGestionarProyectos gestProy=new DBGestionarProyectos();
			LinkedList<String>proyectos=gestProy.leerProyectos(dl.getEmailLogin());
			String mensaje="Proyecto Borrado";
			model.addObject("proyectos", proyectos);
			model.addObject("mensaje", mensaje);
			return model;
		}
		DBGestionarProyectos gestProy=new DBGestionarProyectos();
		LinkedList<String>proyectos=gestProy.leerProyectos(dl.getEmailLogin());
		model.addObject("proyectos", proyectos);
		return model;
	}
	
}
