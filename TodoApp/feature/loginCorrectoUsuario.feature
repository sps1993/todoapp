Feature: loginCorrectoUsuario

	Scenario: Login Correcto
		Given Introducir usuario y clave correcto
		When Pulsar Login
		Then Acceder a pantalla principal de usuario
		
	Scenario: Login Incorrecto
		Given Introducir usuario o clave incorrecto
		When Pulsar Login
		Then Mostrar un Mensaje de Error