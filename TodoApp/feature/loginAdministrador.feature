Feature: loginCorrectoAdministrador

	Scenario: Login Correcto
		Given Introducir usuario y clave correcto admin
		When Pulsar Login admin
		Then Acceder a pantalla principal de administrador
		
	Scenario: Login Incorrecto
		Given Introducir usuario o clave incorrecto admin
		When Pulsar Login
		Then Mostrar un Mensaje de Error administrador