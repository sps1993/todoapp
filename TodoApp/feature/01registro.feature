Feature: Registro

	Scenario: Registro Correcto
		Given Un usuario se registra con email y clave
		When La cuenta se crea
		Then Mostrar Mensaje OK
	
	Scenario: Cuenta Existente
		Given Un usuario se registra con email y clave
		When La cuenta existe
		Then Mostrar Mensaje de Error

	Scenario: Contraseņa Corta
		Given Un usuario se registra con email y clave
		When La clave tiene menos de 8 caracteres
		Then Mostrar Mensaje de Error
	