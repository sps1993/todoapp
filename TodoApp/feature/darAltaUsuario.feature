Feature: darAltaUsuario

	Scenario: El administrador da de alta a un Usuario
		Given El admin introduce su email y pass
		When Pulsar en entrar
			And Acceder a pantalla principal de administrador
			And pulsa en darAlta
		Then selecciona un usuario
			And le da de Alta
			
	Scenario: El administrador da de alta a varios Usuarios
		Given El admin introduce su email y password
		When Pulsar en entrar para loguearse
			And Accede a su pantalla principal de administrador
			And pulsa en Altas de Usuarios
		Then selecciona varios usuario
			And les da de Alta