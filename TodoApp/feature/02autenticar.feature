Feature: Autenticar

	Scenario: Autenticacion Correcta
		Given Un usuario introduce email y clave
		When Autenticacion es correcta
		Then Mostrar Mensaje OK
		
	Scenario: Autenticacion Incorrecta
		Given Un usuario introduce mal email y clave
		When Autenticacion es incorrecta
		Then Mostrar Mensaje de Error