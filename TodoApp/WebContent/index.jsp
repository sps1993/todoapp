<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
</head>
<body>
<table>
		<tr>
			<th colspan="6">
				<h3 id="header">
				TODO APP
				</h3>
			</th>
		</tr>
	</table>
	<form action="entrar.htm" method="post">
	<table>
		<tr>
			<td colspan="2">
				<p>Usuario/email</p>
			</td>
			<td>
				<input class="form-control" name="userEmailLogin" type="text" id="userEmailLogin">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p>Contraseña</p>
			</td>
			<td>
				<input class="form-control" name="contrasenaLogin" type="password" id="contrasenaLogin">
			</td>
		</tr>
	</table>

	<table>
	
		<tr>
			<td>
					<p><a href="http://localhost:8080/TodoApp/InterfazRecuperarContrasena.htm" id="RecuperarContrasena">Recuperar clave</a></p>
			</td>
		</tr>

		<tr>
			<td>
				
					<button type="submit" class="btn btn-default" id="botonEntrar">
						Entrar
					</button>
			</td>
		</tr>
	</table>
</form>

	<form action="registrar.htm" method="post">
		<button type="submit" class="btn btn-default" id="botonRegistrar">
			Registrar
		</button>
	</form>

<p>${mensaje}</p>
</body>
</html>