<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

	<title>TODO APP - Registrar</title>
	
</head>
  <body>
  	<table>
		<tr>
			<th colspan="6">
				<h3 id="header">
				TODO APP
				</h3>
			</th>
		</tr>
	</table>

	<table>
		<tr>
			<th colspan="6">
				<h3>
				Registrar
				</h3>
			</th>
		</tr>
	</table>

	<form action="reg.htm" method="post">
		<table>
			<tr>
				<td colspan="2">
					<p><b>Email</b></p>
				</td>
				<td>
					<input class="form-control" name="userEmailReg" id="userEmailRegRegistrar" type="text">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<p><b>Contraseña</b></p>
				</td>
				<td>
					<input class="form-control" name="contrasenaReg" id="contrasenaRegRegistrar" type="password">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<p><b>Repetir contraseña</b></p>
				</td>
				<td>
					<input class="form-control" name="contrasenaReg2" id="contrasena2RegRegistrar" type="password">
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" class="btn btn-default" id="aceptarRegistrar">
						Aceptar
					</button>
				</td>
			</tr>
		</table>
	</form>
	<p>${mensaje}</p>
  </body>
</html>