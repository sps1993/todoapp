<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
	 <title>TODO APP - 	Info. Tarea</title>
  </head>
  <body>

  	<table>
  		<tr>
  			<th>
  				<form action="principalUser.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="menuPrincipalCambiarContrasena">
						Principal
					</button>
				</form>
  			</th>
  			<th>
  				<form action="creaProyecto.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearProyectoCambiarContrasena">
						Crear Proyecto
					</button>
				</form>
  			</th>
  			<th>
  				<form action="crearTarea.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearTareaCambiarContrasena">
						Crear Tarea
					</button>
				</form>
  			</th>
  			<th>
  				<form action="cambiarContraseña.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="cambiarContrasenaCambiarContrasena">
						Cambiar Contraseña
					</button>
				</form>
  			</th>
  			<th>
  				<form action="verPerfilo.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="verPerfilCambiarContrasenas">
						   Ver Perfil
					 </button>
				</form>
			</th>
			<th>
          		<form action="cerrarSesionUser.htm"	method="post">
	          		<button type="submit" class="btn btn-default" id="cerrarSesionCambiarContrasena">
	             			Cerrar Sesión
	          		</button>
	          	</form>
  			</th>
  		</tr>
  	</table>
    
  <form action="volverTareasUsuario.htm" method="post">
    <table>
  		<tr>
  			<td colspan="1">
  				<p><b>Nombre</b></p>
  			</td>
  			<td colspan="5">
				  <input class="form-control" id="inputNombreInformacionTarea" name="nombreTar" type="text" value="${nombreTar}" disabled>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<p><b>Prioridad</b></p>
  			</td>
  			<td>
				<select id="selectPrioridadInformacionTarea" name="prioridadTar"  disabled>
  					<option value=value="${prioridadTar}>"${prioridadTar}"</option>
				</select>
  			</td>
  			<td>
  				<p><b>Proyecto</b></p>
  			</td>
  			<!--LEER LOS TIPOS DE PROYECTOS DESDE LOS .JAVA -->
  			<td>
				<select id="selectProyectoInformacionTarea" name="proyectoTar" disabled>
  					<option value="${proyectoTar}">"${proyectoTar}"</option>
				</select>
  			</td>
  			<td colspan="2">
				  <input type="checkbox" name="completadaInformacionTarea" id="completadaInformacionTarea" checked="${completadaInformacionTarea}"> Completada<br>
  			</td>
  		</tr>
  		<tr>
  			<td>
				  <p><b>Fecha Inicio: </b></p>
  			</td>
  			<td colspan="2">
				  <input type="datetime-local" id="fechaInicioInformacionTarea" name="fInicioTar" value="${fInicioTar}" disabled>
  			</td>
  			<td>
				  <p><b>Fecha Límite:</b></p>
  			</td>
  			<td colspan="2">
				  <input type="datetime-local" id="fechaFinInformacionTarea" name="fLimiteTar" value="${fLimiteTar}" disabled>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<p><b>Notas</b></p>
  			</td>
  			<td colspan="6">
  				<input class="form-control" id="inputNotasInformacionTarea" name="getNotasTar" value="${getNotasTar}" type="text" disabled>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<button type="submit" class="btn btn-default" id="aceptarInformacionTarea">
					 Volver
				  </button>
  			</td>
  		</tr>
  	</table>
  	</form>
  </body>
</html>