<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Cambiar Roles</title>
  </head>
<body>
	<table>
  		<tr>
  			<th>
  			<form action="pantallaprincipaladministrador.htm" method="post">
  				<button type="submit" class="btn btn-default" id="menuPrincipalCambiarRoles">
					Principal
				</button>
			</form>
  			</th>
  			<th>
  			<form action="altausuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="darAltasCambiarRoles">
					Altas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="bajasdeusuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="darBajasCambiarRoles">
					Bajas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  				<button type="submit" class="btn btn-default" id="cambiarRolesOptionCambiarRoles" disabled>
					Cambiar Roles
				</button>
  			</th>
  			<th>
  			<form action="listadeusuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="listarUsuariosCambiarRoles">
					Listado Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="verPerfilLogueado.htm" method="post">
  				<button type="submit" class="btn btn-default" id="verPerfilCambiarRoles">
					   Ver Perfil
				  </button>
			</form>
			</th>
			<th>
			<form action="pantallainicio.htm" method="post">
          		<button type="submit" class="btn btn-default" id="cerrarSesionCambiarRoles">
             		Cerrar Sesión
          		</button>
          	</form>
  			</th>
  		</tr>
    </table>

    <table>
    	<tr>
    		<th>
    			<h2>Lista de usuarios</h2>
    		</th>
    	</tr>
    </table>

	<form action="cambiar.htm" method="post">
	    <table>
	    	<tr>
	    		<th></th>
	    		<th>Usuario</th>
	    		<th>Rol</th>
	    	</tr>
	    	<c:forEach items="${lista}" var="datosUsuario" varStatus="i">
      		<tr>
      			<td>
      				<c:set var="nombre" value="checkbox${i.index}"></c:set>
      				<input type='checkbox' name='${nombre}' />
      			</td>
      		<c:forEach items="${datosUsuario}" var="item">
      			<c:forEach items="${item}" var="i">
      					<td>
      						<c:out value="${i}" />
      					</td>
      			</c:forEach>
      		</c:forEach>
      		</tr>
      </c:forEach>
	    </table>
	
	
	    <table>
	    	<tr>
	    		<td>
	    			<p><b>Seleccionar rol</b></p>
	    		</td>
	    		<td>
	    			<select name="rol">
						<option id="usuario">Usuario</option>
						<option id="admin">Admin</option>
					</select>
	    		</td>
	    	</tr>
	    	<tr>
	    		<td>
	    			<button type="submit" class="btn btn-default" id="guardarCambiarRoles">Guardar Cambios</button>
	    		</td>
	    	</tr>
	    </table>
    
    </form>

	</body>
</html>