<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
	<title>TODO APP - Recuperar Contraseña</title>
	
</head>
  <body>

    <table>
      <tr>
        <h3 id="header">
          TODO APP
        </h3>
      </tr>
    </table>
<form action ="RecuperarContraseña.htm" method="post">
    <table>
      <tr>
        <td><p><b>Email</b></p></td>
        <td>
          <input type="email" class="form-control" placeholder="Email usuario"  id ="inputEmailRecuperarContrasena" name="inputEmail">
        </td>
      </tr>
      <tr>
        <td>
          <button type="submit" class="btn btn-default" id ="recuperarClaveRecuperarContrasena">
              Recuperar Clave
          </button>
        </td> 
      </tr>
    </table>
    </form>
<form action="menuprincipal_home.htm" method="post">
	<table>
		<tr>
			<td>
				 <button type="submit" class="btn btn-default" id ="volverHomeRecuperarContrasena">
              		Volver al menú principal
          		</button>
			</td>
		</tr>
	</table>
</form>
<p>${mensaje}</p>
  </body>
</html>