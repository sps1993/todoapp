<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
	<title>TODO APP - Nuevo Proyecto</title>
  </head>
  <body>

    <table>
      <tr>
        <h3 id="header">
          TODO APP
        </h3>
      </tr>
    </table>

	<table>
  		<tr>
  			<th>
  				<form action="principalUser.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="menuPrincipalPrincipalUsuarios">
						Principal
					</button>
				</form>
  			</th>
  			<th>
  				<form action="creaProyecto.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearProyectoPrincipalUsuarios">
						Crear Proyecto
					</button>
				</form>
  			</th>
  			<th>
  				<form action="crearTarea.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearTareaPrincipalUsuarios">
						Crear Tarea
					</button>
				</form>
  			</th>
  			<th>
  				<form action="cambiarContraseña.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="cambiarContrasenaPrincipalUsuarios">
						Cambiar Contraseña
					</button>
				</form>
  			</th>
  			<th>
  				<form action="verPerfilo.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="verPerfilPrincipalUsuarios">
						   Ver Perfil
					 </button>
				</form>
			</th>
			<th>
          		<form action="cerrarSesionUser.htm"	method="post">
	          		<button type="submit" class="btn btn-default" id="cerrarSesionPrincipalUsuarios">
	             			Cerrar Sesión
	          		</button>
	          	</form>
  			</th>
  		</tr>
  	</table>

    <table>
      <tr>
        <td colspan="2"><p><b>Nuevo proyecto</b></p></td>
        <td>
          <input type="text" class="form-control" name ="inputNombreProyecto" id ="inputNombreProyectoNuevoProyecto" value="">
        </td>
      </tr>
      <tr>
        <td>
          <button type="submit" class="btn btn-default" id ="guardarNuevoProyecto">
              Guardar
          </button>
        </td> 
      </tr>
    </table>
  </body>
</html>