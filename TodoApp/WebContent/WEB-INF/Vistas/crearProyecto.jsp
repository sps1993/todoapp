<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Los import -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Listar Usuarios</title>
				
	</head>
	<body>
	
	<table>
  		<tr>
  			<th>
  				<form action="principalUsuario.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="menuPrincipalCrearProyecto">
						Principal
					</button>
				</form>
  			</th>
  			<th>
  				
	  				<button type="submit" class="btn btn-default" id="crearProyectoCrearProyecto" disabled>
						Crear Proyecto
					</button>
				
  			</th>
  			<th>
  				<form action="crearTareas.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearTareaCrearProyecto">
						Crear Tarea
					</button>
				</form>
  			</th>
  			<th>
  				<form action="cambiarContrasena.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="cambiarContrasenaCrearProyecto">
						Cambiar Contraseña
					</button>
				</form>
  			</th>
  			<th>
  				<form action="verPerfilUser.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="verPerfilCrearProyecto">
						   Ver Perfil
					 </button>
				</form>
			</th>
			<th>
          		<form action="cerrarSesionUsuario.htm"	method="post">
	          		<button type="submit" class="btn btn-default" id="cerrarSesionCrearProyecto">
	             			Cerrar Sesión
	          		</button>
	          	</form>
  			</th>
  		</tr>
    </table>

	<form action="crear.htm" method="post">

    <table>
    	<tr>
    		<th>Proyectos Creados</th>
    	</tr>
    	
    	<c:forEach items="${proyectos}" var="nProyecto" varStatus="i">
      		<tr>
      			
      		<c:forEach items="${nProyecto}" var="item">
      					<c:forEach items="${item}" var="i">
      					<td>
      					    <c:set var="nombre" value='${i}'></c:set>
      						<c:out value="${nombre}" />
      					</td>
      			</c:forEach>
      		</c:forEach>
      		</tr>
      </c:forEach>
      <tr>
				<td colspan="2">
					<p><b>Nuevo Proyecto</b></p>
				</td>
				<td>
					<input class="form-control" name="nuevoProyecto" id="nuevoProyectoCrearProyecto" type="text">
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" class="btn btn-default" id="crearProyectoCrearProyecto">
						Crear Proyecto
					</button>
				</td>
			</tr>
    </table>
    
    </form>
    
    <form action="borrarProyecto.htm" method="post">
    		<input class="form-control" name="nombreProyectoBorrarCrearProyecto" id="nombreProyectoBorrarCrearProyecto" type="text">
			<button type="submit" class="btn btn-default" id="crearProyectoCrearProyecto">
				Borrar Proyecto
			</button>
    
    </form>
    <p>${mensaje}</p>
	</body>
</html>