<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Home Admin</title>
  </head>
  <body>
	<table>
  		<tr>
  			<th>
  				<button type="submit" class="btn btn-default" id="menuPrincipalPrincipalAdmin" disabled>
					Principal
				</button>
  			</th>
  			<th>
  			<form action="darAltas.htm" method="post">
  				<button type="submit" class="btn btn-default" id="darAltasPrincipalAdmin">
					Altas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="darBajas.htm" method="post">
  				<button type="submit" class="btn btn-default" id="darBajasPrincipalAdmin">
					Bajas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="cambiarRoles.htm" method="post">
  				<button type="submit" class="btn btn-default" id="cambiarRolesPrincipalAdmin">
					Cambiar Roles
				</button>
			</form>
  			</th>
  			<th>
  			<form action="listarUsuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="listarUsuariosPrincipalAdmin">
					Listado Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="verPerfilAdmin.htm" method="post">
  				<button type="submit" class="btn btn-default" id="verPerfilPrincipalAdmin">
					   Ver Perfil
				  </button>
			</form>
			</th>
			<th>
			<form action="index.htm" method="post">
          		<button type="submit" class="btn btn-default" id="cerrarSesionPrincipalAdmin">
             		Cerrar Sesión
          		</button>
          	</form>
  			</th>
  		</tr>
    </table>
  </body>
</html>