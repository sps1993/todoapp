<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Dar de Alta</title>
	
  </head>
  
  <body>

  	<table>
  		<tr>
  			<th>
  			<form action="PantallaPrincipal.htm" method="post" id="menuPrincipalDarDeAlta">
  				<button type="submit" class="btn btn-default">
					Principal
				</button>
			</form>
  			</th>
  			<th>
  				<button type="submit" class="btn btn-default" id="darAltasDarDeAlta" disabled>
					Altas de Usuarios
				</button>
  			</th>
  			<th>
  			<form action="darDeBaja.htm" method="post">
  				<button type="submit" class="btn btn-default" id="darBajasDarDeAlta">
					Bajas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="cambiarRolesUsuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="cambiarRolesDarDeAlta">
					Cambiar Roles
				</button>
			</form>
  			</th>
  			<th>
  			<form action="listadoUsuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="listarUsuariosDarDeAlta">
					Listado Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="verperfil.htm" method="post">
  				<button type="submit" class="btn btn-default" id="verPerfilDarDeAlta">
					   Ver Perfil
				  </button>
			</form>
			</th>
			<th>
			<form action="Home.htm" method="post">
          		<button type="submit" class="btn btn-default" id="cerrarSesionDarDeAlta">
             		Cerrar Sesión
          		</button>
          	</form>
  			</th>
  		</tr>
    </table>

    <table>
    	<tr>
    		<th>
    			<h2>
				Dar de alta
				</h2>
    		</th>	
    	</tr>
    </table>
    
	<form action="Dar_de_Alta.htm" method="post">

    <table>
    	<tr>
			<th></th>
        	<th>E-mail</th>
        	<th>Nombre</th>
      </tr>
      	<c:forEach items="${lista}" var="datosUsuario" varStatus="i">
      		<tr>
      			<td>
      				<c:set var="nombre" value="checkbox${i.index}" ></c:set>
      				<input type='checkbox' name='${nombre}' id='${nombre}'/>
      			</td>
      		<c:forEach items="${datosUsuario}" var="item">
      			<c:forEach items="${item}" var="i">
      					<td>
      						<c:out value="${i}" />
      					</td>
      			</c:forEach>
      		</c:forEach>
      		</tr>
      </c:forEach>
		<tr>
			<td>
					<button class="btn btn-default" type="submit" id="darDeAltaUsuarioDarDeAlta">
						Dar de alta
					</button>
					

			</td>
		</tr>
    </table>
    </form>
  </body>
</html>
