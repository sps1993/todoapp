<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
	<title>TODO APP - Cambiar contraseña</title>
	
</head>
<body>

	<table>
  		<tr>
  			<th>
  				<form action="principalUser.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="menuPrincipalCambiarContrasena">
						Principal
					</button>
				</form>
  			</th>
  			<th>
  				<form action="creaProyecto.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearProyectoCambiarContrasena">
						Crear Proyecto
					</button>
				</form>
  			</th>
  			<th>
  				<form action="crearTarea.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearTareaCambiarContrasena">
						Crear Tarea
					</button>
				</form>
  			</th>
  			<th>
  				<form action="cambiarContraseña.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="cambiarContrasenaCambiarContrasena" disabled>
						Cambiar Contraseña
					</button>
				</form>
  			</th>
  			<th>
  				<form action="verPerfilo.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="verPerfilCambiarContrasenas">
						   Ver Perfil
					 </button>
				</form>
			</th>
			<th>
          		<form action="cerrarSesionUser.htm"	method="post">
	          		<button type="submit" class="btn btn-default" id="cerrarSesionCambiarContrasena">
	             			Cerrar Sesión
	          		</button>
	          	</form>
  			</th>
  		</tr>
  	</table>
<form action="Cambiar_Contrasena.htm" method="post">
	<table>
		<tr>
			<th colspan="2">
				<h4>Cambio de contraseña</h4>
			</th>
		</tr>
		<tr>
			<td colspan="2">
				<p>Escribe tu contraseña actual</p>
			</td>
			<td>
				<input class="form-control" id="contrasenaActualCambiarContrasena" name ="contrasenaActual" type="password">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p>Escribe tu nueva contraseña</p>
			</td>
			<td>
				<input class="form-control" id="contrasenaNuevaCambiarContrasena" name="contrasenaNueva" type="password">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p>Vuelve a escribir tu nueva contraseña</p>
			</td>
			<td>
				<input class="form-control" id="contrasenaActualRepCambiarContrasena" name="contrasenaNuevaRep" type="password">
			</td>
		</tr>
		<tr>
			<td>
				<button type="submit" class="btn btn-default" id="botonGuardarCambiarContrasena">
					Guardar
				</button>
			</td>
		</tr>
	</table>
<p>${feedback}</p>
	</form>
</body>
</html>