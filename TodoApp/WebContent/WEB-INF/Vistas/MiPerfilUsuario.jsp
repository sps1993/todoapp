<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Mi Perfil</title>
  </head>
  <body>
	<table>
  		<tr>
  			<th>
  				<form action="principalUser.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="menuPrincipalPrincipalUsuarios">
						Principal
					</button>
				</form>
  			</th>
  			<th>
  				<form action="creaProyecto.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearProyectoPrincipalUsuarios">
						Crear Proyecto
					</button>
				</form>
  			</th>
  			<th>
  				<form action="crearTarea.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearTareaPrincipalUsuarios">
						Crear Tarea
					</button>
				</form>
  			</th>
  			<th>
  				<form action="cambiarContraseña.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="cambiarContrasenaPrincipalUsuarios">
						Cambiar Contraseña
					</button>
				</form>
  			</th>
  			<th>
  				<form action="verPerfilo.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="verPerfilPrincipalUsuarios" disabled>
						   Ver Perfil
					 </button>
				</form>
			</th>
			<th>
          		<form action="cerrarSesionUser.htm"	method="post">
	          		<button type="submit" class="btn btn-default" id="cerrarSesionPrincipalUsuarios">
	             			Cerrar Sesión
	          		</button>
	          	</form>
  			</th>
  		</tr>
  	</table>
  	<form action="modificarPerfil.htm" method="post">
    <table>
    <c:set var="emailUser" value="${email}" scope="session"/> 
      <tr>
        <th>
          <h3>Perfil de '${emailUser}'</h3>
        </th>
      </tr>
    </table>
    <table>
      <tr>
        <td>
          <p><b>Nombre</b></p>
        </td>
        <td>
          <input type="text" class="form-control" name="inputNombre" id="inputNombreMiPerfilUsuario" value="${nombre}" disabled >
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Apellidos</b></p>
        </td>
        <td>
          <input type="text" class="form-control" name="inputApellidos" id="inputApellidosMiPerfilUsuario" value="${apellidos}" disabled>
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Dirección</b></p>
        </td>
        <td>
          <input type="text" class="form-control" name="inputDireccion" id="inputDireccionMiPerfilUsuario" value="${direccion}" disabled>
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Teléfono</b></p>
        </td>
        <td>
          <input type="text" class="form-control" name="inputTelefono" id="inputTelefonoMiPerfilUsuario" value="${telefono}" disabled>
        </td>
      </tr>
            <tr>
        <td>
  			<button type="submit" class="btn btn-default" id="modificarMiPerfilUsuario">
				Modificar
			</button>
  		</td>
      </tr>
    </table>
    </form>
    
    <form action="eliminarCuenta.htm" method="post">
    
    	<button type="submit" class="btn btn-default" id="borrarCuentaUsuario">
				Borrar Cuenta
		</button>

    </form>
  </body>
</html>
