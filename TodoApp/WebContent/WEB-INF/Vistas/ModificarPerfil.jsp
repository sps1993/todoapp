<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Modificar Perfil</title>
  </head>
  <body>
    <table>
  		<tr>
  			<th>
  			<form action="PantallaPrincipal.htm" method="post" id="menuPrincipalDarDeAlta">
  				<button type="submit" class="btn btn-default">
					Principal
				</button>
			</form>
  			</th>
  			<th>
  				<form action="altasusuariosadmin.htm" method = "post">
  				<button type="submit" class="btn btn-default" id="darAltasDarDeAlta" disabled>
					Altas de Usuarios
				</button>
				</form>
  			</th>
  			<th>
  			<form action="darDeBaja.htm" method="post">
  				<button type="submit" class="btn btn-default" id="darBajasDarDeAlta">
					Bajas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="cambiarRolesUsuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="cambiarRolesDarDeAlta">
					Cambiar Roles
				</button>
			</form>
  			</th>
  			<th>
  			<form action="listadoUsuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="listarUsuariosDarDeAlta">
					Listado Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="verperfil.htm" method="post">
  				<button type="submit" class="btn btn-default" id="verPerfilDarDeAlta">
					   Ver Perfil
				  </button>
			</form>
			</th>
			<th>
			<form action="Home.htm" method="post">
          		<button type="submit" class="btn btn-default" id="cerrarSesionDarDeAlta">
             		Cerrar Sesión
          		</button>
          	</form>
  			</th>
  		</tr>
    </table>
	<form action="perfilModificar.htm" method="post">
    <table>
    
    <c:set var="emailAdmin" value="${email}" scope="session"/>
      <tr>
        <th>
          <h3>Modificar Perfil de '${emailAdmin}'</h3>
        </th>
      </tr>
    </table>
    <table>
      <tr>
        <td>
          <p><b>Nombre</b></p>
        </td>
        <td>
          <input type="text" class="form-control" id="inputNombreModificarPerfil" name="inputNombre" value="${nombre}">
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Apellidos</b></p>
        </td>
        <td>
          <input type="text" class="form-control" id="inputApellidosModificarPerfil" name="inputApellidos" value="${apellidos}">
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Dirección</b></p>
        </td>
        <td>
          <input type="text" class="form-control" id="inputDireccionModificarPerfil" name="inputDireccion" value="${direccion}">
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Teléfono</b></p>
        </td>
        <td>
          <input type="text" class="form-control" id="inputTelefonoModificarPerfil" name="inputTelefono" value="${telefono}">
        </td>
      </tr>
      <tr>
        <td>
          <button type="submit" class="btn btn-default" id="guardarModificarPerfil">Guardar Cambios</button>
        </td>
      </tr>
    </table>
  </body>
</html>
