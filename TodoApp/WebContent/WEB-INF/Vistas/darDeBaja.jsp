<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Dar de Baja</title>
	
  </head>
  
  <body>

  	<table>
  		<tr>
  			<th>
  			<form action="PrincipalAdmin.htm" method="post">
  				<button type="submit" class="btn btn-default" id="menuPrincipalDarDeBaja">
					Principal
				</button>
			</form>
  			</th>
  			<th>
  			<form action="AltasUsuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="darAltasDarDeBaja">
					Altas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  				<button type="submit" class="btn btn-default" id="darBajasDarDeBaja" disabled>
					Bajas de Usuarios
				</button>
  			</th>
  			<th>
  			<form action="cambioRoles.htm" method="post">
  				<button type="submit" class="btn btn-default" id="cambiarRolesDarDeBaja">
					Cambiar Roles
				</button>
			</form>
  			</th>
  			<th>
  			<form action="listarusers.htm" method="post">
  				<button type="submit" class="btn btn-default" id="listarUsuariosDarDeBaja">
					Listado Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="vermiperfil.htm" method="post">
  				<button type="submit" class="btn btn-default" id="verPerfilDarDeBaja">
					   Ver Perfil
				  </button>
			</form>
			</th>
			<th>
			<form action="login.htm" method="post">
          		<button type="submit" class="btn btn-default" id="cerrarSesionDarDeBaja">
             		Cerrar Sesión
          		</button>
          	</form>
  			</th>
  		</tr>
    </table>

    <table>
    	<tr>
    		<th>
    			<h2>
				Dar de baja
				</h2>
    		</th>	
    	</tr>
    </table>

	<form action="Dar_Bajas.htm" method="post">

    <table>
    	<tr>
			<th></th>
        	<th>E-mail</th>
        	<th>Nombre</th>
      </tr>
      	<c:forEach items="${lista}" var="datosUsuario" varStatus="i">
      		<tr>
      			<td>
      				<c:set var="nombre" value="checkbox${i.index}"></c:set>
      				<input type='checkbox' name='${nombre}' id='${nombre}'/>
      			</td>
      		<c:forEach items="${datosUsuario}" var="item">
      			<c:forEach items="${item}" var="i">
      					<td>
      						<c:out value="${i}" />
      					</td>
      			</c:forEach>
      		</c:forEach>
      		</tr>
      </c:forEach>
		<tr>
			<td>
					<button class="btn btn-default" type="submit" id="darDeBajaUsuarioDarDeBaja">
						Dar de baja
					</button>
					

			</td>
		</tr>
    </table>
    </form>
  </body>
</html>