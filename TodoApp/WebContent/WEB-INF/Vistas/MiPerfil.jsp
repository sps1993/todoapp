<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Mi Perfil</title>
  </head>
  <body>
    <table>
      <tr>
        <th>
        <form action="pantallaprincipaladmin.htm" method="post">
          	<button type="submit" class="btn btn-default" id="menuPrincipalMiPerfil">
          		Principal
        	</button>
        </form>
        </th>
        <th>
        <form action="daraltashomeadmin.htm" method="post">
          	<button type="submit" class="btn btn-default" id="darAltasMiPerfil">
          		Altas de Usuarios
        	</button>
        </form>
        </th>
        <th>
        <form action="darbajashomeadmin.htm" method="post">
          	<button type="submit" class="btn btn-default" id="darBajasMiPerfil">
          		Bajas de Usuarios
        	</button>
        </form>
        </th>
        <th>
        <form action="modRolUsers.htm" method="post">
          	<button type="submit" class="btn btn-default" id="cambiarRolesMiPerfil">
          		Cambiar Roles
        	</button>
        </form>
        </th>
        <th>
        <form action="listadodeusuarios.htm" method="post">
          	<button type="submit" class="btn btn-default" id="listarUsuariosMiPerfil">
          		Listado Usuarios
        	</button>
        </form>
        </th>
        <th>
          <button type="button" class="btn btn-default" id="verPerfilOptionMiPerfil" disabled>
             Ver Perfil
          </button>
        </th>
       	<th>
       	<form action="PantallaHome.htm" method="post">
          <button type="submit" class="btn btn-default" id="cerrarSesionMiPerfil">
             Cerrar Sesión
          </button>
        </form>
        </th>
      </tr>
    </table>
    
    
	<form action="modificar.htm" method="post">

    <table>

	<c:set var="emailAdmin" value="${email}" scope="session"/>    	
      <tr>
        <th>
          <h3>Perfil de '${emailAdmin}'</h3>
        </th>
      </tr>
    </table>
    <table>
      <tr>
        <td>
          <p><b>Nombre</b></p>
        </td>
        <td>
          <input type="nombre" class="form-control" name="inputNombre" id="inputNombreMiPerfil" value="${nombre}" disabled>
        </td>
       </tr>
      <tr>
        <td>
          <p><b>Apellidos</b></p>
        </td>
        <td>
          <input type="nombre" class="form-control" name="inputApelidos" id="inputApellidosMiPerfil" value="${apellidos}" disabled>
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Dirección</b></p>
        </td>
        <td>
          <input type="nombre" class="form-control" name="inputDireccion" id="inputDireccionMiPerfil" value="${direccion}" disabled>
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Teléfono</b></p>
        </td>
        <td>
          <input type="nombre" class="form-control" name="inputTelefono" id="inputTelefonoMiPerfil" value="${telefono}" disabled>
        </td>
      </tr>

      <tr>
        <td>
          <button type="submit" class="btn btn-default" id="modificarMiPerfil">Modificar</button>
        </td>
      </tr>
    </table>
    </form>
    
    <form action="eliminarCuentaAdmin.htm" method="post">
    
    	<button type="submit" class="btn btn-default" id="borrarCuentaAdmin">
				Borrar Cuenta
		</button>

    </form>
    
  </body>
</html>
