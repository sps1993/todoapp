<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<!-- Los import -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
	 <title>TODO APP - Crear Tarea</title>
  </head>
  <body>

  	<table>
  		<tr>
  			<th>
  			<form action="homeUser.htm" method="post">
  				<button type="submit" class="btn btn-default" id="menuPrincipalCrearTarea">
					Principal
				</button>
			</form>
  			</th>
  			<th>
  			<form action="crearProyecto.htm" method="post">
  				<button type="submit" class="btn btn-default" id="crearProyectoCrearTarea">
					Crear Proyecto
				</button>
			</form>
  			</th>
  			<th>
  				<button type="submit" class="btn btn-default" id="crearTareaOptionCrearTarea" disabled>
					Crear Tarea
				</button>
  			</th>
  			<th>
  			<form action="cambiarPass.htm" method="post">
  				<button type="submit" class="btn btn-default" id="cambiarContrasenaCrearTarea">
					Cambiar Contraseña
				</button>
			</form>
  			</th>
  			<th>
  			<form action="verPerfilUsuario.htm" method="post">
  				<button type="submit" class="btn btn-default" id="verPerfilCrearTarea">
					   Ver Perfil
				 </button>
			</form>
			</th>
			<th>
			<form action="logoutTodoApp.htm" method="post">
          		<button type="submit" class="btn btn-default" id="cerrarSesionCrearTarea">
             			Cerrar Sesión
          		</button>
          	</form>
  			</th>
  		</tr>
  	</table>
  	<form action="tareaNueva.htm" method="post">
    <table>
  		<tr>
  			<td colspan="1">
  				<p><b>Nombre</b></p>
  			</td>
  			<td colspan="5">
				  <input class="form-control" id="inputNombreCrearTarea" name="nombreCT" type="text">
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<p><b>Prioridad</b></p>
  			</td>
  			<td>
				<select id="selectPrioridadCrearTarea" name="prioridadCT">
  					<option value="urgente">Urgente</option>
  					<option value="media">Media</option>
  					<option value="baja">Baja</option>
				</select>
  			</td>
  			<td>
  				<p><b>Proyecto</b></p>
  			</td>
  			<!--LEER LOS TIPOS DE PROYECTOS DESDE LOS .JAVA -->
  			<td>
				<select id="selectProyectoCrearTarea" name="proyectoCT">
					<c:forEach items="${proyectos}" var="nProyecto" varStatus="i">
					<c:forEach items="${nProyecto}" var="item">
						<c:forEach items="${item}" var="i">
							<c:set var="nombreProyecto" value='${i}'></c:set>
							<option value="${nombreProyecto}">
								<c:out value="${nombreProyecto}" />
							</option>
						</c:forEach>
					</c:forEach>
				</c:forEach>
				</select>
  			</td>
  			<td colspan="2">
				  <input type="checkbox" name="completadaCrearTarea" value="completada" id="completadaCrearTarea" unchecked> Completada<br>
  			</td>
  		</tr>
  		<tr>
  			<td>
				  <p><b>Fecha Inicio: </b></p>
  			</td>
  			<td colspan="2">
				  <input type="datetime-local" id="fechaInicioCrearTarea" name="fechaInicioCT" placeholder="yyyy/mm/dd">
  			</td>
  			<td>
				  <p><b>Fecha Límite:</b></p>
  			</td>
  			<td colspan="2">
				  <input type="datetime-local" id="fechaFinCrearTarea" name="fechaFinCT" placeholder="yyyy/mm/dd">
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<p><b>Notas</b></p>
  			</td>
  			<td colspan="6">
  				<input class="form-control" id="inputNotasCrearTarea" name="notasCT" type="text">
  			</td>
  		</tr>
  		<tr>
  			<td colspan="4"></td>
  			<td>
  			    <p>${feedback}</p>
  				<button type="submit" class="btn btn-default" id="guardarCrearTarea">
					 Guardar
				  </button>
  			</td>
  		</tr>
  	</table>
  	</form>
  </body>
</html>