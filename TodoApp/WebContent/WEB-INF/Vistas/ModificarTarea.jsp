<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">
	 <title>TODO APP - 	Info. Tarea</title>
  </head>
  <body>

  	<table>
  		<tr>
  			<th>
  			<form action="principal.htm" method="post">
  				<button type="button" class="btn btn-default" id="menuPrincipalModificarTarea">
					Principal
				</button>
			</form>
  			</th>
  			<th>
  			<form action="crearProyecto.htm" method="post">
  				<button type="button" class="btn btn-default" id="crearProyectoModificarTarea">
					Crear Proyecto
				</button>
			</form>
  			</th>
  			<th>
  			<form action="crearTarea.htm" method="post">
  				<button type="button" class="btn btn-default" id="crearTareaModificarTarea">
					Crear Tarea
				</button>
			</form>
  			</th>
  			<th>
  			<form action="cambiarContrasena.htm" method="post">
  				<button type="button" class="btn btn-default" id="cambiarContrasenaModificarTarea">
					Cambiar Contrase�a
				</button>
			</form>
  			</th>
  			<th>
  			<form action="verPerfil.htm" method="post">
  				<button type="button" class="btn btn-default" id="verPerfilModificarTarea">
					   Ver Perfil
				 </button>
			</form>
			<form action="cerrarSesion.htm" method="post">
          		<button type="button" class="btn btn-default" id="cerrarSesionModificarTarea">
             			Cerrar Sesi�n
          		</button>
          	</form>
  			</th>
  		</tr>
  	</table>
    <table>
  		<tr>
  			<td colspan="1">
  				<p><b>Nombre</b></p>
  			</td>
  			<td colspan="5">
				  <input class="form-control" name="inputNombre" id="inputNombreModificarTarea" type="email" disabled>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<p><b>Prioridad</b></p>
  			</td>
  			<td>
				<select id="selectPrioridadModificarTarea">
  					<option value="Urgente">Urgente</option>
  					<option value="media">Media</option>
  					<option value="baja">Baja</option>
				</select>
  			</td>
  			<td>
  				<p><b>Proyecto</b></p>
  			</td>
  			<!--LEER LOS TIPOS DE PROYECTOS DESDE LOS .JAVA -->
  			<td>
				<select id="selectProyectoModificarTarea">
  					<option value="proyecto1">Proyecto 1</option>
  					<option value="proyecto2">Proyecto 2</option>
  					<option value="proyecto3">Proyecto 3</option>
				</select>
  			</td>
  			<td colspan="2">
				  <input type="radio" name="completada" id="completadaModificarTarea" value="completada" unchecked> Completada<br>
  			</td>
  		</tr>
  		<tr>
  			<td>
				  <p><b>Fecha Inicio: </b></p>
  			</td>
  			<td colspan="2">
				  <input type="datetime-local" id="fechaInicioModificarTarea" value="">
  			</td>
  			<td>
				  <p><b>Fecha L�mite:</b></p>
  			</td>
  			<td colspan="2">
				  <input type="datetime-local" id="fechaFinModificarTarea" value="">
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<p><b>Notas</b></p>
  			</td>
  			<td colspan="6">
  				<input class="form-control" id="inputNotasModificarTarea" type="text">
  			</td>
  		</tr>
  		<tr>
  			<td colspan="4"></td>
  			<td>
  				<button type="button" class="btn btn-default" id="aceptarModificarTarea">
					 Aceptar
				</button>
  			</td>
  		</tr>
  	</table>
  </body>
</html>