<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Modificar Perfil</title>
  </head>
  <body>
	<form action="perfilUsuarioModificar.htm" method="post">
    <table>
    <c:set var="emailUser" value="${email}" scope="session"/> 
      <tr>
        <th>
          <h3>Modificar Perfil de '${emailUser}'</h3>
        </th>
      </tr>
    </table>
    
    <form action="perfilUsuarioModificar.htm" method="post">
    
    <table>
      <tr>
        <td>
          <p><b>Nombre</b></p>
        </td>
        <td>
          <input type="nombre" class="form-control" name="inputNombre" id="inputNombreModificarPerfilUsuario" value="${nombre}">
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Apellidos</b></p>
        </td>
        <td>
          <input type="nombre" class="form-control" name="inputApellidos" id="inputApellidosModificarPerfilUsuario" value="${apellidos}">
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Direcci�n</b></p>
        </td>
        <td>
          <input type="nombre" class="form-control" name="inputDireccion" id="inputDireccionModificarPerfilUsuario" value="${direccion}">
        </td>
      </tr>
      <tr>
        <td>
          <p><b>Tel�fono</b></p>
        </td>
        <td>
          <input type="nombre" class="form-control" name="inputTelefono" id="inputTelefonoModificarPerfilUsuario" value="${telefono}">
        </td>
      </tr>
      <tr>
        <td>
          <button type="submit" class="btn btn-default" id="guardarModificarPerfilUsuario">Guardar Cambios</button>
        </td>
      </tr>
    </table>
    </form>
  </body>
</html>
