<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Los import -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Listar Usuarios</title>
				
	</head>
	<body>
	
	<table>
  		<tr>
  			<th>
  			<form action="principaladministrador.htm" method="post">
  				<button type="submit" class="btn btn-default" id="menuPrincipalListarUsuarios">
					Principal
				</button>
			</form>
  			</th>
  			<th>
  			<form action="altasusuariosadmin.htm" method="post">
  				<button type="sumbit" class="btn btn-default" id="darAltasListarUsuarios">
					Altas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="bajasusuarios.htm" method="post">
  				<button type="submit" class="btn btn-default" id="darBajasListarUsuarios">
					Bajas de Usuarios
				</button>
			</form>
  			</th>
  			<th>
  			<form action="modificarroles.htm" method="post">
  				<button type="submit" class="btn btn-default" id="cambiarRolesListarUsuarios">
					Cambiar Roles
				</button>
			</form>
  			</th>
  			<th>
  				<button type="submit" class="btn btn-default" id="listarUsuariosOptionListarUsuarios" disabled>
					Listado Usuarios
				</button>
  			</th>
  			<th>
  			<form action="verAdminPerfil.htm" method="post">
  				<button type="submit" class="btn btn-default" id="verPerfilListarUsuarios">
					   Ver Perfil
				  </button>
			</form>
			</th>
			<th>
			<form action="PantallaPrincipalTodoApp.htm" method="post">
          		<button type="submit" class="btn btn-default" id="cerrarSesionListarUsuarios">
             		Cerrar Sesión
          		</button>
          	</form>
  			</th>
  		</tr>
    </table>

	

    <table>
    	<tr>
    		<th>Email</th>
    	</tr>
    	
    	<c:forEach items="${listaTodos}" var="datosUsuario" varStatus="i">
      		<tr>
      			<c:set var="nombreBotones" value="boton${i.index}"></c:set>
      			<c:set var="indice" value="${i.index}"></c:set>
      		<c:forEach items="${datosUsuario}" var="item">
      					<c:forEach items="${item}" var="i">
      					<td>
      					    <c:set var="nombre" value='${i}'></c:set>
      						<c:out value="${nombre}" />
      					</td>
      			</c:forEach>
      			<form action="ver.htm" method="post">
      			      		    <td>
      				<button class="btn btn-default" type="submit" name='${nombreBotones}' value='${nombreBotones}' id='${nombreBotones}'>Ver Perfil Usuario</button> 
      			</td>
      			 </form>
      			 <form action="verTareas.htm" method="post">
      			      		    <td>
      				<button class="btn btn-default" type="submit" name='${nombreBotones}' value='${nombreBotones}' id='${nombreBotones}'>Ver Tareas Usuario</button> 
      			</td>
      			 </form>
      			<td>
      			</td>
      		</c:forEach>
      		</tr>
      </c:forEach>
    </table>
	</body>
</html>