<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!-- Los import -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es" xml:lang="es" id="pruebaVista">
  <head>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vista referente a TODOAPP realizada por el grupo de Trello SPS">
    <meta name="author" content="SPS Team">

    <title>TodoAPP - Ver Tareas Usuarios </title>
	
  </head>
  <body>
  	<table>
  		<tr>
  			<th>
  				<form action="principalUser.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="menuPrincipalPrincipalUsuarios" disabled>
						Principal
					</button>
				</form>
  			</th>
  			<th>
  				<form action="creaProyecto.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearProyectoPrincipalUsuarios">
						Crear Proyecto
					</button>
				</form>
  			</th>
  			<th>
  				<form action="crearTarea.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="crearTareaPrincipalUsuarios">
						Crear Tarea
					</button>
				</form>
  			</th>
  			<th>
  				<form action="cambiarContraseña.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="cambiarContrasenaPrincipalUsuarios">
						Cambiar Contraseña
					</button>
				</form>
  			</th>
  			<th>
  				<form action="verPerfilo.htm" method="post">
	  				<button type="submit" class="btn btn-default" id="verPerfilPrincipalUsuarios">
						   Ver Perfil
					 </button>
				</form>
			</th>
			<th>
          		<form action="cerrarSesionUser.htm"	method="post">
	          		<button type="submit" class="btn btn-default" id="cerrarSesionPrincipalUsuarios">
	             			Cerrar Sesión
	          		</button>
	          	</form>
  			</th>
  		</tr>
  	</table>
    
    <table>
  		<tr>
  			<td>
  				<h2>Administrador de Tareas</h2>
  			</td>
  		</tr>
    </table>
    
    <form action="filtrarTablas.htm" method="post">
    	<table>
  		<tr><th><h3>Ordenación:</h3></th></tr>
  			<tr>
  			<td>
  				<button type="submit" class="btn btn-default" name="filtroProyectoBoton" id="filtroProyectoPrincipalUsuarios">
	             		Ordenar por Proyecto
	          	</button>
  			</td>
  			<td>
  				<button type="submit" class="btn btn-default" name="filtroPrioridadBoton" id="filtroPrioridadPrincipalUsuarios">
	             		Ordenar por Prioridad
	          	</button>
  			</td>
  			<td>
  				<button type="submit" class="btn btn-default" name="filtroFechaBoton" id="filtroFechaPrincipalUsuarios">
	             		Ordenar por Fecha
	          	</button>
  			</td>
  		</tr>
    </table>
    </form>
    
    
  		<form action="verInfoTarea.htm" method="post">

    <table>
    	<tr>
    		<th><p><b>Tareas Completadas</b></p></th>
    	</tr>
    	<tr>
    		<th></th>
    		<th><p><b>Nombre</b></p></th>
    		<th><p><b>Fecha fin</b></p></th>
    		<th><p><b>Proyecto</b></p></th>
    		<th><p><b>Prioridad</b></p></th>
    	</tr>
    	<c:forEach items="${tareasCompletas}" var="datosTarea" varStatus="i">
      		<tr>
      			<td>
      			<c:set var="nombreBotones" value="boton${i.index}tareasCompletadas"></c:set>
      				<c:set var="nombre" value="checkbox${i.index}tareasCompletadas"></c:set>
      				<input type='checkbox' name='${nombre}' id='${nombre}'/>
      			</td>
      		<c:forEach items="${datosTarea}" var="item">
      			<c:forEach items="${item}" var="i">
      					<td>
      						<c:out value="${i}" />
      					</td>
      					
      			</c:forEach>
      			
      		</c:forEach>
      		<td>
      				<button class="btn btn-default" type="submit" name='${nombreBotones}' value='${nombreBotones}' id='${nombreBotones}'>Ver información tarea</button> 
      			</td>
      		</tr>
      		      		
      </c:forEach>
    		
    </table>

    <table>
    	<tr>
    		<th><p><b>Tareas Actuales</b></p></th>
    	</tr>
    	<tr>
    		<th></th>
    		<th><p><b>Nombre</b></p></th>
    		<th><p><b>Fecha fin</b></p></th>
    		<th><p><b>Proyecto</b></p></th>
    		<th><p><b>Prioridad</b></p></th>
    	</tr>
    	<c:forEach items="${tareasActuales}" var="datosTarea" varStatus="i">
      		<tr>
      			<td>
      			<c:set var="nombreBotones" value="boton${i.index}tareasActuales"></c:set>
      				<c:set var="nombre" value="checkbox${i.index}tareasActuales"></c:set>
      				<input type='checkbox' name='${nombre}' id='${nombre}'/>
      			</td>
      		<c:forEach items="${datosTarea}" var="item">
      			<c:forEach items="${item}" var="i">
      					<td>
      						<c:out value="${i}" />
      					</td>
      			</c:forEach>
      			
      		</c:forEach>
      		<td>
      				<button class="btn btn-default" type="submit" name='${nombreBotones}' value='${nombreBotones}' id='${nombreBotones}'>Ver información tarea</button> 
      			</td>
      		</tr>
      </c:forEach>
    	
    		
    </table>

    <table>
    	<tr>
    		<th><p><b>Tareas Futuras</b></p></th>
    	</tr>
    	<tr>
    		<th></th>
    		<th><p><b>Nombre</b></p></th>
    		<th><p><b>Fecha fin</b></p></th>
    		<th><p><b>Proyecto</b></p></th>
    		<th><p><b>Prioridad</b></p></th>
    	</tr>
    	<c:forEach items="${tareasFuturas}" var="datosTarea" varStatus="i">
      		<tr>
      			<td>
      			<c:set var="nombreBotones" value="boton${i.index}tareasFuturas"></c:set>
      				<c:set var="nombre" value="checkbox${i.index}tareasFuturas"></c:set>
      				<input type='checkbox' name='${nombre}' id='${nombre}'/>
      			</td>
      		<c:forEach items="${datosTarea}" var="item">
      			<c:forEach items="${item}" var="i">
      					<td>
      						<c:out value="${i}" />
      					</td>
      			</c:forEach>
      			
      		</c:forEach>
      		<td>
      				<button class="btn btn-default" type="submit" name='${nombreBotones}' value='${nombreBotones}' id='${nombreBotones}'>Ver información tarea</button> 
      			</td>
      		</tr>
      </c:forEach>
    		

    </table>  		
    <table>
    		<td>
				<button class="btn btn-default" type="submit" id="borrarPrincipalUsuarios" name="borrarPrincipalUsuarios">
					Borrar
				</button> 
			</td>
		
    </table>
        </form>
  </body>
</html>