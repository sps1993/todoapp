# TodoAPP - Versión grupo SPS #
## Práctica 1 de Procesos de la Ingeniería del Software ##
### Universidad de Castilla - La Mancha ###

### Autores: ###
* Sergio Pérez Sánchez
* Marcos Sánchez Iglesias
* Silvia Alonso Izquierdo
* Jesús Elvira Sánchez
* Salvador Palomino Prieto

### Descripción: ###
Repositorio destinado a la aplicación web TodoAPP, encargada de la gestión de tareas

### Links de interés: ###
* Google Drive con la documentación: [PISO 2016 - Grupo SPS](https://drive.google.com/drive/folders/0BxuvsWVNlG_beWRSLUJIaDJOeWs)

* Pagina Web Aplicacion(https://sites.google.com/view/todoapp-sps/p%C3%A1gina-principal)